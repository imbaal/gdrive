CREATE TABLE public.t_doctor_office_schedule
(
    id bigserial NOT NULL,
    doctor_id bigint,
    medical_facility_schedule_id bigint,
    slot integer,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL DEFAULT FALSE,
    PRIMARY KEY (id)
);

ALTER TABLE IF EXISTS public.t_doctor_office_schedule
    OWNER to postgres;

CREATE TABLE public.t_doctor_office_treatment
(
    id bigserial NOT NULL,
    doctor_treatment_id bigint,
    doctor_office_id bigint,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL DEFAULT FALSE,
    PRIMARY KEY (id)
);

ALTER TABLE IF EXISTS public.t_doctor_office_treatment
    OWNER to postgres;

CREATE TABLE public.t_doctor_office_treatment_price
(
    id bigserial NOT NULL,
    doctor_office_treatment_id bigint,
    price real,
    price_start_from real,
    price_until_from real,
    created_by bigint NOT NULL,
    created_on date NOT NULL,
    modified_by bigint,
    modified_on date,
    deleted_by bigint,
    deleted_on date,
    is_delete boolean NOT NULL DEFAULT FALSE,
    PRIMARY KEY (id)
);

a1b2c3d4e5
ALTER TABLE IF EXISTS public.t_doctor_office_treatment_price
    OWNER to postgres;

SELECT * FROM public.m_biodata;
SELECT * FROM public.m_specialization;
SELECT * FROM public.m_doctor;
SELECT * FROM public.t_current_doctor_specialization;
SELECT * FROM public.t_doctor_treatment
SELECT * FROM public.m_location_level;
SELECT * FROM public.m_location;
SELECT * FROM public.m_medical_facility_category;
SELECT * FROM public.m_medical_facility;
SELECT * FROM public.t_doctor_office;

SELECT * FROM public.t_doctor_office;
SELECT * FROM public.m_medical_facility;
SELECT * FROM public.m_medical_facility_schedule;
SELECT * FROM public.t_doctor_office_schedule
SELECT * FROM public.t_doctor_office_treatment
SELECT * FROM public.t_doctor_office_treatment_price



SELECT * FROM public.m_education_level;
SELECT * FROM public.m_doctor_education;


SELECT Md.id, Mb.fullname, mmf.id AS medical_facility_id, Mmfc.name AS category, Mmf.name AS faskes_name, 
Tdo.specialization, Ml.name AS location, Mmf.full_address, Mmfs.id AS jadwal_id,
        Mmfs.day, Mmfs.time_schedule_start, Mmfs.time_schedule_end
		FROM public.m_doctor Md
        JOIN public.m_biodata Mb ON Md.biodata_id = Mb.id
        JOIN public.t_doctor_office Tdo ON Tdo.doctor_id = Md.id
        JOIN public.m_medical_facility Mmf ON Mmf.id = Tdo.medical_facility_id
        JOIN public.m_medical_facility_category Mmfc ON Mmfc.id = Mmf.medical_facility_category_id
        JOIN public.m_location Ml ON Ml.id = Mmf.location_id
		JOIN public.m_medical_facility_schedule Mmfs ON Mmfs.medical_facility_id = Mmf.id
        WHERE  Tdo.is_delete = FALSE


SELECT md.id, mmf.id as idrs, Mmfs.id AS jadwal_id,Mmfs.day, Mmfs.time_schedule_start, Mmfs.time_schedule_end
From m_doctor md
Join t_doctor_office tdo
on tdo.doctor_id=md.id
JOIN m_medical_facility mmf
ON mmf.id = Tdo.medical_facility_id
JOIN public.m_medical_facility_schedule Mmfs 
ON Mmfs.medical_facility_id = Mmf.id



	SELECT * FROM public.t_doctor_office_schedule
	SELECT * FROM public.t_doctor_office_treatment
	SELECT * FROM public.t_doctor_office_treatment_price

SELECT 
FROM 
JOIN
ON
JOIN
ON
JOIN
ON
JOIN
ON

SELECT * FROM public.m_biodata;
SELECT * FROM public.m_specialization;
SELECT * FROM public.m_doctor;
SELECT * FROM public.t_current_doctor_specialization;
SELECT * FROM public.t_doctor_treatment
SELECT * FROM public.m_location_level;
SELECT * FROM public.m_location;
SELECT * FROM public.m_medical_facility_category;
SELECT * FROM public.m_medical_facility;
SELECT * FROM public.t_doctor_office;

SELECT * FROM public.t_doctor_office;
SELECT * FROM public.m_medical_facility;
SELECT * FROM public.m_medical_facility_schedule;
SELECT * FROM public.t_doctor_office_schedule
SELECT * FROM public.t_doctor_office_treatment
SELECT * FROM public.t_doctor_office_treatment_price


SELECT Md.id, Mb.fullname, mmf.id AS medical_facility_id, Mmfc.name AS category, Mmf.name AS faskes_name, Mmf.id as idrs,
        Tdo.specialization, Ml.name AS location, Mmf.full_address
                FROM public.m_doctor Md
                JOIN public.m_biodata Mb ON Md.biodata_id = Mb.id
                JOIN public.t_doctor_office Tdo ON Tdo.doctor_id = Md.id
                JOIN public.m_medical_facility Mmf ON Mmf.id = Tdo.medical_facility_id
                JOIN public.m_medical_facility_category Mmfc ON Mmfc.id = Mmf.medical_facility_category_id
                JOIN public.m_location Ml ON Ml.id = Mmf.location_id
                WHERE Tdo.is_delete = FALSE and md.id=1
                ORDER BY md.id asc
				
				SELECT md.id, mmf.id as idrs, Mmfs.id AS jadwal_id,Mmfs.day, Mmfs.time_schedule_start, Mmfs.time_schedule_end
                From m_doctor md
                Join t_doctor_office tdo
                on tdo.doctor_id=md.id
                JOIN m_medical_facility mmf
                ON mmf.id = Tdo.medical_facility_id
                JOIN public.m_medical_facility_schedule Mmfs 
                ON Mmfs.medical_facility_id = Mmf.id
                WHERE md.id=1
SELECT Md.id, Mb.fullname, mmf.id AS medical_facility_id, Mmfc.name AS category, Mmf.name AS faskes_name, Mmf.id as idrs,
        Tdo.specialization, Ml.name AS location, Mmf.full_address
                FROM public.m_doctor Md
                JOIN public.m_biodata Mb ON Md.biodata_id = Mb.id
                JOIN public.t_doctor_office Tdo ON Tdo.doctor_id = Md.id
                JOIN public.m_medical_facility Mmf ON Mmf.id = Tdo.medical_facility_id
                JOIN public.m_medical_facility_category Mmfc ON Mmfc.id = Mmf.medical_facility_category_id
                JOIN public.m_location Ml ON Ml.id = Mmf.location_id
                WHERE Tdo.is_delete = FALSE
