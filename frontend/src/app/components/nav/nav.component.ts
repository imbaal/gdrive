import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { MBiodata } from 'src/app/models/m_biodata';
import { MUser } from 'src/app/models/m_user';
import { CObat } from 'src/app/models/t_cariobat';
import { LoginService } from 'src/app/services/auth/login.service';
import { RoleService } from 'src/app/services/m_role/role.service';
import { TCariobatService } from 'src/app/services/t_cariobat/t-cariobat.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  public form: any = {
    email: null,
    password: null,
    kodeOtp: null,
    setPassword: null,
    repeatPassword: null
  };

  public login: any = [];
  public isLoggedIn = false;
  public isRegisterFailed = false;
  public isLoginFailed = false;
  public pesan = "";
  public e = "";
  public penelusuran = false
  public hiden = false

  // var registrasi
  public cekEmail = true;
  public kodeOtp = false;
  public setPassword = false;
  public daftarGodoc = false;
  public currentEmail: any;
  public currentSetPassword: any;
  public currentRepeatPassword: any;

  //iqbal
  public keranjang: any = []
  public pilihankategori: any = []
  public page=1
  //iqbal

  public role: any = [];

  public subcription: Subscription;
  public user: MUser = {} as MUser;

  constructor(private loginService: LoginService, private router: Router, private roleService: RoleService, private cariobatService: TCariobatService) {
    this.subcription = this.loginService.currentUser.subscribe( //
      (newUser) => {
        (this.user = newUser)
        console.log(newUser)
      }
    )
  }

  ngOnInit(): void {
    this.dataawalpopup()
  }

  /* SHOW PASSWORD */
  public fieldTextType = false;

  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }
  toggleFieldTextType2() {
    this.fieldTextType = !this.fieldTextType;
  }

  /* REGISTER */

  kirimOtp(): void {
    const { email } = this.form;

    this.loginService.registrasiService(email).subscribe(
      (response: any) => {
        this.isRegisterFailed = false
        this.cekEmail = false;
        this.kodeOtp = true;
        this.setPassword = false;
        this.daftarGodoc = false;

        this.currentEmail = email;
        console.log(email);
      },
      (error: HttpErrorResponse) => {
        this.isRegisterFailed = true
        this.e = 'Email sudah terdaftar'
        console.log(this.e)
      }
    )
  }

  verifikasiOtp(): void {
    const { kodeOtp } = this.form;

    this.loginService.verifyKodeOtp(kodeOtp).subscribe(
      (response: any) => {
        this.cekEmail = false;
        this.kodeOtp = false;
        this.setPassword = true;
        this.daftarGodoc = false;
      },
      (error: HttpErrorResponse) => {
        this.isRegisterFailed = true
        this.e = 'Kode OTP Salah'
        console.log(this.e)
      }
    )
  }

  setPasswords(): void {
    const { setPassword, repeatPassword } = this.form;

    if (setPassword === repeatPassword) {
      this.currentSetPassword = setPassword;
      this.currentRepeatPassword = repeatPassword;
      console.log(this.currentSetPassword);
      console.log(this.currentRepeatPassword);


      this.cekEmail = false;
      this.kodeOtp = false;
      this.setPassword = false;
      this.daftarGodoc = true;

      this.getRole();

    } else {
      this.isRegisterFailed = true
      this.e = 'Password tidak sama'
      console.log(this.e);
    }
  }

  public getRole(): void {
    this.roleService.getRole().subscribe(
      (response: any) => {
        this.role = response.result
      },
      (error: HttpErrorResponse) => {
        alert(error.message)
      }
    )
  }

  public daftarAkun(daftarForm: NgForm): void {
    const closeRegistrasi = document.getElementById('closeRegistrasi')

    console.log(daftarForm.value);

    this.loginService.daftarAkun(daftarForm.value, this.currentEmail, this.currentSetPassword).subscribe(
      (response: MBiodata) => {
        // this.getRole();
        daftarForm.reset();
        closeRegistrasi?.click();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        daftarForm.reset();
      }
    )
  }

  /* LOGIN */

  onSubmit(): void {
    const close = document.getElementById('close-login')
    const { email, password } = this.form;

    this.loginService.loginService(email, password).subscribe(
      (response: any) => {
        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.loginService.setUser(response.result);

        close?.click();
      },
      (error: HttpErrorResponse) => {
        this.isLoginFailed = true;
        this.pesan = "email atau password salah"
        console.log(this.pesan)
      }
    );
  }

  onReset() {
    this.form = {
      email: null,
      password: null
    };
  }

  onLogout() {
    this.isLoggedIn = false
    this.loginService.setUser({} as MUser)
    this.onReset();
    this.router.navigate(['/home'])
  }
  public openCariObatModal(c: CObat, mode: string): void {
    console.log(mode)
    const container = document.getElementById('main-container')
    const button = document.createElement('button')


    button.type = 'button'
    button.style.display = 'none'
    button.setAttribute('data-bs-toggle', 'modal')


    if (mode == 'CariObat') {
      button.setAttribute('data-bs-target', '#cariobatModal')
    }

    if (mode == 'UlangCari') {
      this.hiden = false
      button.setAttribute('data-bs-target', '#cariobatModal')
    }
    if (mode == 'akhirpopup') {
      this.penelusuran = true
      console.log('berhasil diubah')
    }
    if (mode == 'detail') {
      button.setAttribute('data-bs-target', '#detailprodukModal')
    }
    container!.appendChild(button)
    button.click()
  }

  public dataawalpopup(): void {
    this.cariobatService.isiawalmodal().subscribe(
      (response: any) => {
        this.pilihankategori = response.result
        console.log(this.pilihankategori)
      }, (error: HttpErrorResponse) => {
        alert(error.message)
      }
    )
  }

  public oncariobat(addForm: NgForm): void {
    this.penelusuran = true
    let t = addForm.value
    if (t.minharga == "") { t.minharga = parseInt('0') }
    if (t.maxharga == "") { t.maxharga = parseInt('9999999999999999')}
    if (t.katakunci == "") {t.katakunci = ''}
    console.log(t)
    this.cariobatService.gantiCariObat(t);
    this.router.navigate(['cariobat'])


  }


}
