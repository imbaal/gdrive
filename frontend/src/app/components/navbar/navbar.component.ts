import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Iqbal } from 'src/app/models/t_customer_wallet';
import { TCariobatService } from 'src/app/services/t_cariobat/t-cariobat.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  public obat :any=[]

  constructor(private cariObatService: TCariobatService) { }

  ngOnInit(): void {
  }

  public onOpenModal(c: Iqbal, mode: string): void {
    const container = document.getElementById('main-container')
    const button = document.createElement('button')

    button.type = 'button'
    button.style.display = 'none'
    button.setAttribute('data-bs-toggle', 'modal')

   
    if (mode == 'cari'){
      button.setAttribute('data-bs-target','#CariObat')
    }
    container!.appendChild(button)
    button.click()
  }

  // public onGetObatGroup():void{
  //   this.cariObatService.getObat().subscribe(
  //     (response:any)=>{
  //       this.obat= response.result
  //     },(error:HttpErrorResponse) =>{
  //       alert(error.message)
  //     }
  //   )
  // }
  // public onSearchObat(cari: any) {
  //   if (cari !== '' && cari !== ' ') {
  //     this.cariObatService.searchObat(cari).subscribe(
  //       (response: any) => {
  //         this.obat = response.result
  //       }, (error: HttpErrorResponse) => {
  //         alert(error.message)
  //       }
  //     )
  //   }
  //   else { this.onGetObatGroup() }
  // }

}
