import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { MUser } from 'src/app/models/m_user';
import { LoginService } from 'src/app/services/auth/login.service';
import { NavigationExtras } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  public subcription: Subscription;
  public user: MUser = {} as MUser;

  constructor(private loginService: LoginService) {
    this.subcription = this.loginService.currentUser.subscribe( //
      (newUser) => {
        (this.user = newUser)
        console.log(newUser)
      }
    )
  }

  ngOnInit(): void {
  }

  
}
