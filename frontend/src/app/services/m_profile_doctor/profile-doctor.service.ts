import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Config } from 'src/app/config/baseurl';
import { MBiodata } from 'src/app/models/m_biodata';
import { MProfileDoctor } from 'src/app/models/m_profile_doctor';

@Injectable({
  providedIn: 'root'
})
export class ProfileDoctorService {

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    }),
  };

  public getMProfileDoctorByID(biodata_id: number): Observable<MProfileDoctor[]> {
    return this.http.get<MProfileDoctor[]>(
      `${Config.url}api/profile_doctor_id/${biodata_id}`,
      this.httpOptions
    )
  }

  public getMDoctorTreatmentByID(id: number): Observable<MProfileDoctor[]> {
    return this.http.get<MProfileDoctor[]>(
      `${Config.url}api/tindakan_medis/${id}`,
      this.httpOptions
    )
  }

  public getMDoctorEducationByID(id: number): Observable<MProfileDoctor[]> {
    return this.http.get<MProfileDoctor[]>(
      `${Config.url}api/pendidikan_dokter/${id}`,
      this.httpOptions
    )
  }

  public getMRiwayatPraktekByID(id: number): Observable<MProfileDoctor[]> {
    return this.http.get<MProfileDoctor[]>(
      `${Config.url}api/riwayat_praktek/${id}`,
      this.httpOptions
    )
  }

  public getMJanjiDoctorByID(id: number): Observable<MProfileDoctor[]> {
    return this.http.get<MProfileDoctor[]>(
      `${Config.url}api/janji_doctor/${id}`,
      this.httpOptions
    )
  }

  public editMProfileDoctorByID(MBiodata: MBiodata): Observable<MBiodata> {
    return this.http.put<MBiodata>(
      Config.url + 'api/profile_doctor/' + MBiodata.biodata_id,
      MBiodata, {
        ...this.httpOptions,
        responseType: 'text' as 'json'
      }
    )
  }

  public editFotoDoctor(id: any, code: any): Observable<void> {
    return this.http.put<void>(
      Config.url + 'api/edit_foto_dokter/' +id,
      { image: code },
      {
        ...this.httpOptions,
        responseType: 'text' as 'json'
      }
    )
  }
}
