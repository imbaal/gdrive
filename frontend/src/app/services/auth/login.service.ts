import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Config } from 'src/app/config/baseurl';
import { MBiodata } from 'src/app/models/m_biodata';
import { MUser } from 'src/app/models/m_user';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private user = new BehaviorSubject({} as MUser) // 
  currentUser = this.user.asObservable();         // dibaca disemua

  constructor(private http: HttpClient) { }

  httpOption = {
    headers: new HttpHeaders({
      'Content-type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  }

  public setUser(user: MUser) {
    this.user.next(user) //fungsi dari behavior subject
  }

  /*----- LOGIN -----*/

  public loginService(email: string, password: string): Observable<any> {
    return this.http.post(
      Config.url + 'api/auth/login', {
      email,
      password
    }, this.httpOption);
  }

  /*----- REGISTRASI -----*/

  public registrasiService(email: string): Observable<any> {
    return this.http.post(
      Config.url + 'api/auth/registrasi', {
      email
    }, this.httpOption);
  }

  /*----- VERIFY OTP -----*/

  public verifyKodeOtp(kodeOtp: string): Observable<any> {
    return this.http.post(
      Config.url + 'api/auth/verifyOtp', {
      kodeOtp
    }, this.httpOption);
  }

  /*----- DAFTAR AKUN -----*/
  public daftarAkun(biodata: any, email: string, password: string): Observable<any> {
    return this.http.post<any>(
      Config.url + `api/auth/daftarAkun`, {
      ...biodata, email: email, password: password
      },
      this.httpOption
    )
  }


  public getUser(): Observable<MUser[]> {
    return this.http.get<MUser[]>(
      `${Config.url}api/category/`,
      this.httpOption
    )
  }
}
