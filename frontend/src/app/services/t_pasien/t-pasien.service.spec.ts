import { TestBed } from '@angular/core/testing';

import { TPasienService } from './t-pasien.service';

describe('TPasienService', () => {
  let service: TPasienService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TPasienService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
