import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Config } from 'src/app/config/baseurl';
import { t_Pasien } from 'src/app/models/t_pasien';

@Injectable({
  providedIn: 'root'
})
export class TPasienService {

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    })
  }

  public getPasien(sortedby: string, mode: string, search: string, currentPage: number, recordPerPage: number): Observable<t_Pasien[]> {
    return this.http.get<t_Pasien[]>(
      Config.url + `api/pasien/?sortedby=${sortedby}&mode=${mode}&search=${search}&currentPage=${currentPage}&recordPerPage=${recordPerPage}`,
      this.httpOptions
    )
  }

  public addPasien(pasien: t_Pasien): Observable<t_Pasien> {
    return this.http.post<t_Pasien> (
      Config.url + 'api/addpasien',
      pasien, {
        ...this.httpOptions,
        responseType: 'text' as 'json',
      }
    )
  }

  public editPasien(pasien: t_Pasien): Observable<t_Pasien> {
    return this.http.put<t_Pasien>(
      Config.url + 'api/updatepasien/' + pasien.id,
      pasien, {
        ...this.httpOptions,
        responseType: 'text' as 'json'
      }
    )
  }

  public deletePasien(id: number): Observable<void> {
    return this.http.put<void>(
      Config.url + 'api/deletepasien/' + id,
      {
        ...this.httpOptions,
        responseType: 'text' as 'json'
      }
    )
  }
}
