import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Config } from 'src/app/config/baseurl';
import { CustomNominal, DefaultNominal, WalletCustomer } from 'src/app/models/t_customer_wallet';

@Injectable({
  providedIn: 'root'
})
export class TCustomerWalletService {

  constructor(private http: HttpClient) { }

  httpOption = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  }
  getValueWallet(id: number): Observable<WalletCustomer> {
    return this.http.get<WalletCustomer>(
      `${Config.url}api/getDataSaldo/${id}`,
      this.httpOption)
  }

  getWalletTambah(customer_id: number, nominal: number): Observable<WalletCustomer> {
    return this.http.post<WalletCustomer>(
      `${Config.url}api/getWallettambah/${customer_id}`,
      {
        'nominal':`${nominal}`,
        ...this.httpOption,
        responseType: 'text' as 'json'
      })
  }
  getWalletTambah1(customer_id: number, nominal: number): Observable<WalletCustomer> {
    return this.http.post<WalletCustomer>(
      `${Config.url}api/getWallettambah1/${customer_id}`,
      {
        'nominal':`${nominal}`,
        ...this.httpOption,
        responseType: 'text' as 'json'
      })
  }

  getNominalDefault(): Observable<DefaultNominal[]> {
    return this.http.get<DefaultNominal[]>(
      `${Config.url}api/nominaldefault`,
      this.httpOption
    )
  }

  getCustomNominal(customer_id: number): Observable<CustomNominal[]> {
    return this.http.get<CustomNominal[]>(
      `${Config.url}api/customnominal/${customer_id}`,
      this.httpOption
    )
  }

  getCusomerId(id: number):Observable<void> {
    return this.http.get<void>(
      `${Config.url}api/getcustomerid/${id}`,this.httpOption
    )
  }

}

