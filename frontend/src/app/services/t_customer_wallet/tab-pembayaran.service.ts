import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Config } from 'src/app/config/baseurl';
import { Iqbal } from 'src/app/models/t_customer_wallet';

@Injectable({
  providedIn: 'root'
})
export class TabPembayaranService {

  constructor(private http: HttpClient) { }

  HttpOptions ={
    headers: new HttpHeaders({
      'Content-type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    })
  }

  public getKartu(): Observable<Iqbal[]> {
    return this.http.get<Iqbal[]>(
      Config.url +'pembayaran', 
      this.HttpOptions
    )
  }


  public getKartubyid(id:number): Observable<Iqbal[]> {
    return this.http.get<Iqbal[]>(
      Config.url +'pembayaran/'+id, 
      this.HttpOptions
    )
  }



  public addKartu (kartu:Iqbal): Observable<Iqbal>{
    return this.http.post<Iqbal>(
      Config.url + 'pembayaran/tambahpembayaran',
      kartu, {
        ...this.HttpOptions,
      responseType: 'text' as 'json'
      }
    )
    
  }

  public deleteKartu(id :number):Observable<void>{
    return this.http.put<void>(
      Config.url+'pembayaran/deletepembayaran/'+id,{
        ...this.HttpOptions,
        responseType: 'text' as 'json'
      }
    )
  }

}
