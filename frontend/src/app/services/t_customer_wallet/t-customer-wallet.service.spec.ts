import { TestBed } from '@angular/core/testing';

import { TCustomerWalletService } from './t-customer-wallet.service';

describe('TCustomerWalletService', () => {
  let service: TCustomerWalletService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TCustomerWalletService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
