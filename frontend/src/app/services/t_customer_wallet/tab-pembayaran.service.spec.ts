import { TestBed } from '@angular/core/testing';

import { TabPembayaranService } from './tab-pembayaran.service';

describe('TabPembayaranService', () => {
  let service: TabPembayaranService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TabPembayaranService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
