import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Config } from 'src/app/config/baseurl';
import { Blood } from 'src/app/models/m_blood_group';

@Injectable({
  providedIn: 'root'
})
export class MBloodGroupService {

  constructor(private http: HttpClient) { }

  HttpOptions = {
    headers: new HttpHeaders({
      'Content-type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    })
  }

  public getBlood(): Observable<Blood[]> {
    return this.http.get<Blood[]>(
      Config.url + 'pasien/golongandarah',
      this.HttpOptions
    )
  }

  public addBlood(blood: Blood): Observable<Blood> {
    return this.http.post<Blood>(
      Config.url + 'pasien/addgolongandarah', blood, {
      ...this.HttpOptions,
      responseType: 'text' as 'json',
    }
    )
  }

  public editBlood(blood: Blood): Observable<Blood> {
    return this.http.put<Blood>(
      Config.url + 'pasien/editgolongandarah/' + blood.id,
      blood, {
      ...this.HttpOptions,
      responseType: 'text' as 'json'
    }
    )
  }

  public searchBlood(kata:any): Observable<Blood[]>{
    return this.http.get<Blood[]>(
      Config.url+ 'pasien/carigolongandarah/'+kata,this.HttpOptions
    )
  }

  public deleteBlood(id: number): Observable<void> {
    return this.http.put<void>(
      Config.url + 'pasien/deletegolongandarah/' + id, {
      ...this.HttpOptions,
      responseType: 'text' as 'json'
    }
    )
  }
}