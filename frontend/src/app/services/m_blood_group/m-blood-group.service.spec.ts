import { TestBed } from '@angular/core/testing';

import { MBloodGroupService } from './m-blood-group.service';

describe('MBloodGroupService', () => {
  let service: MBloodGroupService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MBloodGroupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
