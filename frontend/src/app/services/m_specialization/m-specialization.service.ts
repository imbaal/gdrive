import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { MSpecialization } from 'src/app/models/m-specialization';
import { Config } from 'src/app/config/baseurl';

@Injectable({
  providedIn: 'root'
})
export class MSpecializationService {

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    }),
  };

  public searchMSpecialization(search : any): Observable<MSpecialization[]> {
    return this.http.get<MSpecialization[]>(
      `${Config.url}api/spesialisasi_dokter/${search}`,
      this.httpOptions
    ) 
  }

  public getMSpecialization(): Observable<MSpecialization[]> {
    return this.http.get<MSpecialization[]>(
      `${Config.url}api/spesialisasi_dokter/`,
      this.httpOptions
    )
  }

  public addMSpecialization(MSpecialization: MSpecialization): Observable<MSpecialization> {
    return this.http.post<MSpecialization>(
      `${Config.url}api/spesialisasi_dokter/`, MSpecialization, {
        ...this.httpOptions,
        responseType: 'text' as 'json',
        }
    )
  }

  public editMSpecialization(MSpecialization: MSpecialization): Observable<MSpecialization> {
    return this.http.put<MSpecialization>(
      Config.url + 'api/ubahspesialisasi_dokter/' + MSpecialization.id,
      MSpecialization, {
        ...this.httpOptions,
        responseType: 'text' as 'json'
      }
    )
  }

  public deleteMSpecialization(id: number): Observable<void> {
    return this.http.put<void>(
      Config.url + 'api/hapusspesialisasi_dokter/' + id,
      {
        ...this.httpOptions,
        responseType: 'text' as 'json'
      }
    )
  }

}
