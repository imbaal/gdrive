import { TestBed } from '@angular/core/testing';

import { MSpecializationService } from './m-specialization.service';

describe('MSpecializationService', () => {
  let service: MSpecializationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MSpecializationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
