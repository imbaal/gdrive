import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Config } from 'src/app/config/baseurl';
import { MRole } from 'src/app/models/m_role';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  constructor(private http: HttpClient) { }

  httpOption = {
    headers: new HttpHeaders({
      'Content-type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  }

  public getRole(): Observable<MRole[]>{
    return this.http.get<MRole[]>(
      `${Config.url}api/role/`,
      this.httpOption
    )
  }

  public getSelectRole(id: number): Observable<MRole[]>{
    return this.http.get<MRole[]>(
      `${Config.url}api/role/` + id,
      this.httpOption
    )
  }
}
