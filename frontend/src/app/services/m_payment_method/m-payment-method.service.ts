import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Config } from 'src/app/config/baseurl';
import { PaymentMethod } from 'src/app/models/m_payment';

@Injectable({
  providedIn: 'root'
})
export class MPaymentMethodService {

  constructor(private http: HttpClient) { }

  httpOption={
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  }
  getJenisPembayaran():Observable<PaymentMethod[]>{
    return this.http.get<PaymentMethod[]>( 
      `${Config.url}api/payment_method`,
       this.httpOption)
  }

  addJenisPembayaran(name:string):Observable<PaymentMethod>{
    return this.http.post<PaymentMethod>(
      `${Config.url}api/payment_method/add/${name}`,
      {
        ... this.httpOption,
        responseType:'text' as 'json'
      }
    )
  }

   editJenisPembayaranid(payment:PaymentMethod):Observable<PaymentMethod>{
    return this.http.put<PaymentMethod>(
      `${Config.url}api/payment_method/update/${payment.id}`,
      payment,{
        ...this.httpOption,
        responseType:'text'as 'json'
      }
    )
  }

  deleteJenisPembayaranid(id:number):Observable<void>{
    return this.http.put<void>(
      `${Config.url}api/payment_method/delete/${id}`,{
        ...this.httpOption,
        responseType:'text' as 'json'
      }
    )
  }
  
  SearchJenisPembayaran(name:string):Observable<PaymentMethod[]>{
    return this.http.get<PaymentMethod[]>(
      `${Config.url}api/payment_method/search/${name}`,
      this.httpOption
    )
  }

  filteredData( sortBy:any,model:string):Observable<PaymentMethod[]>{
    return this.http.get<PaymentMethod[]>( 
      `${Config.url}api/filtered/${sortBy}/${model}`,
      this.httpOption
    )
  }

}
