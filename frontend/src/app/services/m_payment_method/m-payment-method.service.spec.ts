import { TestBed } from '@angular/core/testing';

import { MPaymentMethodService } from './m-payment-method.service';

describe('MPaymentMethodService', () => {
  let service: MPaymentMethodService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MPaymentMethodService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
