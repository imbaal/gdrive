import { TestBed } from '@angular/core/testing';

import { MBankService } from './m-bank.service';

describe('MBankService', () => {
  let service: MBankService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MBankService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
