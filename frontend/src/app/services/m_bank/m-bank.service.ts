import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Config } from 'src/app/config/baseurl';
import { Observable } from 'rxjs';
import { m_Bank } from 'src/app/models/m_bank';

@Injectable({
  providedIn: 'root'
})
export class MBankService {

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    })
  }

  public getBank(): Observable<m_Bank[]> {
    return this.http.get<m_Bank[]>(
      `${Config.url}api/bank`,
      this.httpOptions
    )
  }

  public searchBank(search: any): Observable<m_Bank[]> {
    return this.http.get<m_Bank[]>(
      Config.url + `api/bank/${search}`,
      this.httpOptions,
    )
  }

  public sortingMode(mode: string): Observable<m_Bank[]> {
    return this.http.get<m_Bank[]>(
      Config.url + `api/banksorting/${mode}`,
      this.httpOptions,
    )
  }

  public pagePagination(): Observable<m_Bank[]> {
    return this.http.get<m_Bank[]>(
      Config.url + `api/pagePagination`,
      this.httpOptions
    )
  }

  public addBank(bank: m_Bank): Observable<m_Bank> {
    return this.http.post<m_Bank> (
      Config.url + 'api/addbank',
      bank, {
        ...this.httpOptions,
        responseType: 'text' as 'json',
      }
    )
  }

  public editBank(bank: m_Bank): Observable<m_Bank> {
    return this.http.put<m_Bank>(
      Config.url + 'api/updatebank/' + bank.id,
      bank, {
        ...this.httpOptions,
        responseType: 'text' as 'json'
      }
    )
  }

  public deleteBank(id: number): Observable<void> {
    return this.http.put<void>(
      Config.url + 'api/deletebank/' + id,
      {
        ...this.httpOptions,
        responseType: 'text' as 'json'
      }
    )
  }
}
