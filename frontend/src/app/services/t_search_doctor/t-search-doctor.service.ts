import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Config } from 'src/app/config/baseurl';
import { Doctor } from 'src/app/models/t_cari_dokter';

@Injectable({
  providedIn: 'root'
})
export class TSearchDoctorService {

  constructor(private http:HttpClient) { }

  httpOption=({
    headers: new HttpHeaders({
      'Content-Type': 'application/json', 
      'Access-Control-Allow-Origin': '*',
    })
  })

  getaComponentDoc(fullname:string,spesialisasi: string,lokasi: string,treatment:string):Observable<void>{
    return this.http.get<void>(
      `${Config.url}api/getdatadokter1?fullname=${fullname}&lokasi=${lokasi}&spesialisasi=${spesialisasi}&treatment=${treatment}`,this.httpOption
    )
  }

  getDataLoc():Observable<void>{
    return this.http.get<void>(
      `${Config.url}api/getdatalocation`,this.httpOption
    )
  }
  getDataSpec():Observable<void>{
    return this.http.get<void>(
      `${Config.url}api/getspec`,this.httpOption
    )
  }

  getTreatment():Observable<void>{
    return this.http.get<void>(
      `${Config.url}api/gettreat`,this.httpOption
    )
  }
 
}
