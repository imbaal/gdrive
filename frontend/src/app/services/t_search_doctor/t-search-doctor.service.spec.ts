import { TestBed } from '@angular/core/testing';

import { TSearchDoctorService } from './t-search-doctor.service';

describe('TSearchDoctorService', () => {
  let service: TSearchDoctorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TSearchDoctorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
