import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Config } from 'src/app/config/baseurl';
import { TDetailDoctor } from 'src/app/models/t_detail_doctor';

@Injectable({
  providedIn: 'root'
})
export class DetailDoctorService {

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    }),
  };

  public getPraktekDoctor(id: number): Observable<TDetailDoctor[]> {
    return this.http.get<TDetailDoctor[]>(
      `${Config.url}api/lokasi_praktek1/${id}`,
      this.httpOptions
    )
  }

}
