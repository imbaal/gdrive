import { TestBed } from '@angular/core/testing';

import { MWalletDefaultNominalService } from './m-wallet-default-nominal.service';

describe('MWalletDefaultNominalService', () => {
  let service: MWalletDefaultNominalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MWalletDefaultNominalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
