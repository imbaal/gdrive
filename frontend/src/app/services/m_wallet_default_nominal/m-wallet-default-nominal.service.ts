import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Config } from 'src/app/config/baseurl';
import { DefaultNominal } from 'src/app/models/t_customer_wallet';

@Injectable({
  providedIn: 'root'
})
export class MWalletDefaultNominalService {

  constructor(private http:HttpClient) {}

  httpOption={
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  }
  
  public addDefaultNominal(defaultNominal:any):Observable<DefaultNominal> {
    return this.http.post<DefaultNominal>(
      `${Config.url}api/adddefaultnominal/${defaultNominal}`,
      {
        ... this.httpOption,
        responseType:'text' as 'json'
      }
    )
  }

  public editDefaultNominal(defaultNominal:DefaultNominal):Observable<DefaultNominal> {
    return this.http.put<DefaultNominal>(
      `${Config.url}api/geteditdefaultnominal/${defaultNominal.id}`, 
      defaultNominal,{
        ... this.httpOption,
        responseType:'text' as 'json'
      }
    )
  }


  public deleteNominalDefault(id:any):Observable<void>{
    return this.http.put<void>(
      `${Config.url}api/deletedefault/${id}`,{
        ...this.httpOption,
        responseType:'text' as 'json'
      }
    )
  }

  public searchDefaultName(nominal:number):Observable<DefaultNominal[]> {
    return this.http.post<DefaultNominal[]>(
      `${Config.url}api/getsearch/${nominal}`, 
      this.httpOption
    )
  }

 
}
