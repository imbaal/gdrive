import { TestBed } from '@angular/core/testing';

import { TCariobatService } from './t-cariobat.service';

describe('TCariobatService', () => {
  let service: TCariobatService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TCariobatService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
