import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Config } from 'src/app/config/baseurl';
import { CObat } from 'src/app/models/t_cariobat';
import { Iqbal } from 'src/app/models/t_customer_wallet';

@Injectable({
  providedIn: 'root'
})
export class TCariobatService {

  private obat = new BehaviorSubject({});
  pObat = this.obat.asObservable()

  constructor(private http: HttpClient) { }

  HttpOptions = {
    headers: new HttpHeaders({
      'Content-type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    })
  }

  public gantiCariObat(obat: any) {
    this.obat.next(obat)
  }

  public getObat(kategori: number, katakunci: string, jenisobat: number, hmin: number, hmax: number,page:number): Observable<void> {
    return this.http.get<void>(
      Config.url + 'cariobat/popup?kategori=' + kategori + '&katakunci=' + katakunci + '&segmentation=' + jenisobat + '&price_max=' + hmax + '&price_min=' + hmin + '&page='+page, this.HttpOptions
    )
  }


  public searchObat(kata: any): Observable<Iqbal[]> {
    return this.http.get<Iqbal[]>(
      Config.url + 'obat/cariobatdanalat' + kata, this.HttpOptions
    )
  }

  public isiawalmodal(): Observable<void> {
    return this.http.get<void>(
      Config.url + 'cariobat/awalpopup', this.HttpOptions
    )
  }
}
