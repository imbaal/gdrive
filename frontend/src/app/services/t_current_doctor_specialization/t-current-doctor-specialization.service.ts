import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Config } from 'src/app/config/baseurl';
import { MProfileDoctor } from 'src/app/models/m_profile_doctor';
import { TSpecialization } from 'src/app/models/t_current_doctor_specialization';

@Injectable({
  providedIn: 'root'
})
export class TCurrentDoctorSpecializationService {

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    }),
  };

  public getTSpecialization(doctor_id: any): Observable<TSpecialization[]> {
    return this.http.get<TSpecialization[]>(
      Config.url + 'api/Tspesialisasi_doctor/' + doctor_id,
      this.httpOptions
    )
  }


  public addTSpecialization(TSpecialization: TSpecialization, id: any): Observable<TSpecialization> {
    return this.http.post<TSpecialization>(
      Config.url + 'api/Tspesialisasi_doctor/', { ...TSpecialization, doctor_id: id }, {
      ...this.httpOptions,
      responseType: 'text' as 'json'
    }
    )
  }


  public editTSpecialization(TSpecialization: TSpecialization, doctor_id: any): Observable<TSpecialization> {
    return this.http.put<TSpecialization>(
      Config.url + 'api/Tspesialisasi_doctor/', { ...TSpecialization, doctor_id },
      {
      ...this.httpOptions,
      responseType: 'text' as 'json'
    }
    )
  }

}
