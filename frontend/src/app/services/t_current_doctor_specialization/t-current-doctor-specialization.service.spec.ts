import { TestBed } from '@angular/core/testing';

import { TCurrentDoctorSpecializationService } from './t-current-doctor-specialization.service';

describe('TCurrentDoctorSpecializationService', () => {
  let service: TCurrentDoctorSpecializationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TCurrentDoctorSpecializationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
