import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MWalletDefaultNominalComponent } from './m-wallet-default-nominal.component';

describe('MWalletDefaultNominalComponent', () => {
  let component: MWalletDefaultNominalComponent;
  let fixture: ComponentFixture<MWalletDefaultNominalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MWalletDefaultNominalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MWalletDefaultNominalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
