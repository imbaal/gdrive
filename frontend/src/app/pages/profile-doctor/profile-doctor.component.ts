import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MProfileDoctor } from 'src/app/models/m_profile_doctor';
import { TSpecialization } from 'src/app/models/t_current_doctor_specialization';
import { LoginService } from 'src/app/services/auth/login.service';
import { ProfileDoctorService } from 'src/app/services/m_profile_doctor/profile-doctor.service';
import { MSpecializationService } from 'src/app/services/m_specialization/m-specialization.service';
import { TCurrentDoctorSpecializationService } from 'src/app/services/t_current_doctor_specialization/t-current-doctor-specialization.service';
import { NavigationExtras } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { MUser } from 'src/app/models/m_user';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-profile-doctor',
  templateUrl: './profile-doctor.component.html',
  styleUrls: ['./profile-doctor.component.css']
})
export class ProfileDoctorComponent implements OnInit {
  public mProfileDoctor: MProfileDoctor = {} as MProfileDoctor
  public MDoctorTreatment: any = [];
  public MDoctorEducation: any = [];
  public MRiwayatPraktek: any = [];
  public jumlah_janji = 0
  public TSpecialization: any = [];
  public MSpecialization: any = [];
  public editTSpecialization: TSpecialization;
  public alertTambah = false;
  public alertUbah = false;
  public isEnableTS = 0;

  public imgUrl: SafeUrl | null = null

  public subcription: Subscription;
  public user: MUser = {} as MUser;

  public biodata_id = 0;
  public id = 0;

  constructor(
    private sanitizer: DomSanitizer,
    private activatedRoute: ActivatedRoute,
    private HttpClient: HttpClient,
    private MProfileDoctorService: ProfileDoctorService,
    private TSpecializationService: TCurrentDoctorSpecializationService,
    private MSpecializationService: MSpecializationService,
    private LoginService: LoginService,) {

    this.editTSpecialization = {} as TSpecialization;

    this.subcription = this.LoginService.currentUser.subscribe( //
      (newUser) => {
        (this.user = newUser)
      }
    )
  }

  ngOnInit(): void {
    this.getMProfileDoctorByIDAPI()
    console.log(this.user);
    
  }

  public editFotoDoctor(event: any) {
    const file = event.target.files[0]
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = () => {
      const decode = reader.result?.toString().split(',')
      console.log(decode![1]);
      
      this.MProfileDoctorService.editFotoDoctor(this.mProfileDoctor.biodata_id, decode![1]).subscribe({
        next: () => this.getMProfileDoctorByIDAPI(),
        error: (error: HttpErrorResponse) => alert(error.message)
      })
    }
  }

  public getMSpecializationAPI(): void {
    this.MSpecializationService.getMSpecialization().subscribe(
      (response: any) => {
        this.MSpecialization = response.result;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public getMProfileDoctorByIDAPI(): void {
    this.MProfileDoctorService.getMProfileDoctorByID(this.user.biodata_id).subscribe(
      (response: any) => {
        this.mProfileDoctor = response.result;
        const image = this.mProfileDoctor.image
        console.log(this.mProfileDoctor);
        this.id = this.mProfileDoctor.id

        this.getMDoctorTreatmentByIDAPI(this.id);
        this.getMDoctorEducationByIDAPI(this.id);
        this.getMRiwayatPraktekByIDAPI(this.id);
        this.getMJanjiDoctorByIDAPI(this.id);
        this.getTSpecializationByIDAPI(this.id);
        this.getMSpecializationAPI();

        fetch('data:image/png;base64,' + image).then(res => res.blob()).then(blob => {
          const file = new Blob([blob], { type: 'application/image' })
          const unsafeImg = URL.createObjectURL(file)
          this.imgUrl = this.sanitizer.bypassSecurityTrustUrl(unsafeImg)
        })
     
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public getMDoctorTreatmentByIDAPI(id: number): void {
    this.MProfileDoctorService.getMDoctorTreatmentByID(this.id).subscribe(
      (response: any) => {
        this.MDoctorTreatment = response.result;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public getMDoctorEducationByIDAPI(id: number): void {
    this.MProfileDoctorService.getMDoctorEducationByID(this.id).subscribe(
      (response: any) => {
        this.MDoctorEducation = response.result;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public getMRiwayatPraktekByIDAPI(id: number): void {
    this.MProfileDoctorService.getMRiwayatPraktekByID(this.id).subscribe(
      (response: any) => {
        this.MRiwayatPraktek = response.result;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public getMJanjiDoctorByIDAPI(id: number): void {
    this.MProfileDoctorService.getMJanjiDoctorByID(this.id).subscribe(
      (response: any) => {
        this.jumlah_janji = response.result.jumlah_janji;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public getTSpecializationByIDAPI(doctor_id: any): void {
    this.TSpecializationService.getTSpecialization(this.id).subscribe(
      (response: any) => {
        this.TSpecialization = response.result;
        this.isEnableTS = this.TSpecialization.length;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }


  public onAddTSpecialization(addForm: NgForm): void {

    document.getElementById('add-TSpecialization-form')?.click();

    this.TSpecializationService.addTSpecialization(addForm.value, this.id).subscribe(
      (response: TSpecialization) => {
        this.getMProfileDoctorByIDAPI();
        this.onOpenModal(null!,'succes')
        
        addForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        addForm.reset();
      }
    )
  }

  public onEditTSpecialization(TSpecialization: TSpecialization): void {

    this.TSpecializationService.editTSpecialization(TSpecialization, this.id).subscribe(
      (response: any) => {
        this.getTSpecializationByIDAPI(this.id);
        this.getMProfileDoctorByIDAPI()
        this.onOpenModal(null!,'succes')
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }

  public onOpenModal(TS: TSpecialization, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button')

    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-bs-toggle', 'modal');

    if (mode === 'add') {
      this.alertTambah = false;
      button.setAttribute('data-bs-target', '#addTSModal');
      this.getMSpecializationAPI();

    }

    if (mode === 'edit') {
      this.alertUbah = false;
      this.editTSpecialization = TS;
      button.setAttribute('data-bs-target', '#editTSModal')
      this.getMSpecializationAPI();
    }

    if (mode === 'foto') {
      button.setAttribute('data-bs-target', '#editFoto');
    }

    if (mode == 'succes'){
      button.setAttribute('data-bs-target', '#success')
    }

    container!.appendChild(button);
    button.click();
  }
}
