import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MBloodGroupComponent } from './m-blood-group.component';

describe('MBloodGroupComponent', () => {
  let component: MBloodGroupComponent;
  let fixture: ComponentFixture<MBloodGroupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MBloodGroupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MBloodGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
