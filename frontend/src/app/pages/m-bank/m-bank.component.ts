import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { m_Bank } from 'src/app/models/m_bank';
import { MBankService } from 'src/app/services/m_bank/m-bank.service';

@Component({
  selector: 'app-m-bank',
  templateUrl: './m-bank.component.html',
  styleUrls: ['./m-bank.component.css']
})
export class MBankComponent implements OnInit {
  public bank: any = [];
  public editBank: m_Bank;
  public deleteBank: m_Bank;
  public mode = 'asc';
  public bankTemp: any = [];
  public currentPage: number = 1;
  public recordPer_Page: number = 5;

  constructor(private mBankService: MBankService) {
    this.editBank = {} as m_Bank;
    this.deleteBank = {} as m_Bank;
  }

  ngOnInit(): void {
    this.getMBankAPI();
  }

  public getMBankAPI(): void {
    this.mBankService.getBank().subscribe(
      (response: any) => {
        this.bank = response.result;
        this.bankTemp = response.result;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }

  public onAddBank(addForm: NgForm): void {
    document.getElementById('add-bank-form')?.click();

    this.mBankService.addBank(addForm.value).subscribe(
      (response: m_Bank) => {
        console.log(response);
        this.getMBankAPI();
        addForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        addForm.reset();
      }
    )
  }

  public onEditBank(bank: m_Bank): void {
    this.mBankService.editBank(bank).subscribe(
      (response: m_Bank) => {
        console.log(response);
        this.getMBankAPI();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }

  public onDeleteBank(id: number): void {
    this.mBankService.deleteBank(id).subscribe(
      () => {
        this.getMBankAPI();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }

  public onSearchBank(search: any): void {
    this.mBankService.searchBank(search).subscribe(
      (response: any) => {
        this.bankTemp = response.result;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }

  public sortingMBank(mode: string) {
    this.mBankService.sortingMode(this.mode).subscribe(
      (response: any) => {
        this.bank = response.result;
        this.bankTemp = [];

        for (let index = ((this.currentPage - 1) * this.recordPer_Page); index < (this.currentPage * this.recordPer_Page) && index < this.bank.length; index++) {
          this.bankTemp.push(this.bank[index])
        }
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }

  public onOpenModal(bank: m_Bank, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');

    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');

    if (mode === 'add') {
      button.setAttribute('data-target', '#addMBankModal');
    }

    if (mode === 'edit') {
      this.editBank = bank;
      button.setAttribute('data-target', '#editMBankModal');
    }

    if (mode === 'delete') {
      this.deleteBank = bank;
      button.setAttribute('data-target', '#deleteMBankModal');
    }

    container!.appendChild(button);
    button.click();
  }
}
