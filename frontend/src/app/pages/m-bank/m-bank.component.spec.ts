import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MBankComponent } from './m-bank.component';

describe('MBankComponent', () => {
  let component: MBankComponent;
  let fixture: ComponentFixture<MBankComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MBankComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MBankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
