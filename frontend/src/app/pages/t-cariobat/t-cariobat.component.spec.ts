import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TCariobatComponent } from './t-cariobat.component';

describe('TCariobatComponent', () => {
  let component: TCariobatComponent;
  let fixture: ComponentFixture<TCariobatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TCariobatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TCariobatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
