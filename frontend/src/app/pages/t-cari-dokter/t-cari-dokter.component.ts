import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { TSearchDoctorService } from 'src/app/services/t_search_doctor/t-search-doctor.service';

@Component({
  selector: 'app-t-cari-dokter',
  templateUrl: './t-cari-dokter.component.html',
  styleUrls: ['./t-cari-dokter.component.css']
})
export class TCariDokterComponent implements OnInit {
  public datdoc: any=[]
  public datloc: any=[]
  public dattreat: any=[]
  public fullname=""
  public spesialisasi=""
  public lokasi=""
  public treatment=""
  public page=1
 
 

  constructor(private docservice: TSearchDoctorService) { }

  ngOnInit( ): void {
    this.onGetDataDoctor()
  }

  onGetDataDoctor(){
    this.docservice.getaComponentDoc(this.fullname,this.spesialisasi,this.lokasi,this.treatment).subscribe(
      (response: any) => {
        this.datdoc=response.result;
        this.hidden=false
        this.page=1
        this.sisa = this.datdoc.length % this.pageSize
        this.totalpage = Math.ceil(this.datdoc.length / this.pageSize)
        if(this.datdoc.length ==0){
          this.dataakhir=0
          this.dataawal=0
        }
      },
      (error: HttpErrorResponse) => {
        alert(error.message)
      }
    )
  }
 
  onGetDataLoc(){
    this.docservice.getDataLoc().subscribe(
      (response: any) => {
        this.datloc=response.result;
      
      },
      (error:HttpErrorResponse)=>{
        alert(error.message)
      }
    )
  }

  public datspec:any=[]
  onGetDataSpec(){
    this.docservice.getDataSpec().subscribe(
      (response: any) => {
        this.datspec=response.result;
        
      },
      (error:HttpErrorResponse)=>{
        alert(error.message)
      }
    )
  }

  onGetTreatment(){
    this.docservice.getTreatment().subscribe(
      (response: any) => {
        this.dattreat=response.result;
      
      },
      (error: HttpErrorResponse)=>{
        alert(error.message)
      }
    )
  }
  openModal(mode:string) {
    const container = document.getElementById('main-container');
    const button = document.createElement ('button');

    button.type = 'button'
    button.style.display = 'none'
    button.setAttribute('data-bs-toggle', 'modal')
    if (mode == 'cari') {
      button.setAttribute('data-bs-target', '#cari')
    }
    container!.appendChild(button)
    button.click()
  }

  public hidden=true
  onReset(){
     this.fullname=""
     this.spesialisasi=""
     this.lokasi=""
     this.treatment=""
     this.hidden=true
    
  }

  getLoc(loc:string){
    this.lokasi=loc
  }
  getNameDoc(name:string){
    this.fullname=name
  }
  getSpesialisasi(spec:string){
    this.spesialisasi=spec
  }
  getTreatment(treat:string){
    this.treatment=treat
  } 
 
 
  pageSize = 2;
  sisa = 0
  totalpage = 0

  dataawal = (this.page - 1) * this.pageSize + 1
  dataakhir = this.page * this.pageSize

  handlePageChange(event: number): void {
    this.page = event;

    this.dataawal = (this.page - 1) * this.pageSize + 1
    this.dataakhir = this.page == this.totalpage ? this.page * this.pageSize - this.sisa : this.page * this.pageSize
  }
 
}
