import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TCariDokterComponent } from './t-cari-dokter.component';

describe('TCariDokterComponent', () => {
  let component: TCariDokterComponent;
  let fixture: ComponentFixture<TCariDokterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TCariDokterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TCariDokterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
