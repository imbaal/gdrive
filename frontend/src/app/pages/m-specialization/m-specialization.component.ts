import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MSpecialization } from 'src/app/models/m-specialization';
import { MSpecializationService } from 'src/app/services/m_specialization/m-specialization.service';

@Component({
  selector: 'app-m-specialization',
  templateUrl: './m-specialization.component.html',
  styleUrls: ['./m-specialization.component.css']
})
export class MSpecializationComponent implements OnInit {
  public MSpecialization: any = [];
  public tempMSpecialization: any = [];
  public editMSpecialization: MSpecialization;
  public deleteMSpecialization: MSpecialization;
  public searchMSpecialization: any = [];
  public current_page = 1;
  public records_per_page = 3;
  public hidden = false
  public pesan = ""
  
  constructor(private MSpecializationService: MSpecializationService) {
    this.editMSpecialization = {} as MSpecialization;
    this.deleteMSpecialization = {} as MSpecialization;
  }

  ngOnInit(): void {
    this.getMSpecializationAPI()
  }

  public getMSpecializationAPI(): void {
    this.MSpecializationService.getMSpecialization().subscribe(
      (response: any) => {
        this.MSpecialization = response.result;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public searchMSpecializationAPI(search: any): void {
    this.MSpecializationService.searchMSpecialization(search).subscribe(
      (response: any) => {
        if(search === '') {
          this.getMSpecializationAPI()
        } else {
          this.MSpecialization = response.result;
        }
      });
  }

  public onAddMSpecialization(addForm: NgForm): void {
    document.getElementById('add-MSpecialization-form')?.click();
    let name = addForm.value.name.toLowerCase()
    this.hidden = false
    let count = 0

    for (let i = 0; i < this.MSpecialization.length; i++) {
      let Spesialisasi = this.MSpecialization[i].name.toLowerCase()
      console.log(name);
      console.log(Spesialisasi);
      
      if (Spesialisasi == name) {
        
        this.pesan = " *Spesialisasi Sudah Terdaftar"
        this.hidden = true
        count += 1
        i = this.MSpecialization.length
      }
    }

    if(count == 0) {
      
      this.MSpecializationService.addMSpecialization(addForm.value).subscribe(
        (response: MSpecialization) => {
          this.getMSpecializationAPI();
          addForm.reset();
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
          addForm.reset();
        }
      )
      document.location.reload()
    }
  
  }

  public onEditMSpecialization(MSpecialization: MSpecialization): void {

    this.MSpecializationService.editMSpecialization(MSpecialization).subscribe(
      (response: MSpecialization) => {
        this.getMSpecializationAPI();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }

  public onDeleteMSpecialization(id: number): void {
    this.MSpecializationService.deleteMSpecialization(id).subscribe(
      () => {
        this.getMSpecializationAPI();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onOpenModal(MS: MSpecialization, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button')

    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-bs-toggle', 'modal');

    if (mode === 'add') {
      button.setAttribute('data-bs-target', '#addMSpecializationModal');
    }

    if (mode === 'edit') {
      this.editMSpecialization = MS;
      button.setAttribute('data-bs-target', '#editMSpecializationModal');
    }

    if (mode === 'delete') {
      this.deleteMSpecialization = MS;
      button.setAttribute('data-bs-target', '#deleteMSpecializationModal');
    }

    container!.appendChild(button);
    button.click();
  }

  public nextPage() {
    console.log()
    if (this.current_page < this.totalPages()) {
      this.current_page++;
      this.getMSpecializationAPI();
    }
  }

  public prevPage() {
    if (this.current_page > 1) {
      this.current_page--;
      this.getMSpecializationAPI();
    }
  }

  public totalPages() {
    return Math.ceil(this.MSpecialization.length / this.records_per_page);
  }

}
