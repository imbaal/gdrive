import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MSpecializationComponent } from './m-specialization.component';

describe('MSpecializationComponent', () => {
  let component: MSpecializationComponent;
  let fixture: ComponentFixture<MSpecializationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MSpecializationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MSpecializationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
