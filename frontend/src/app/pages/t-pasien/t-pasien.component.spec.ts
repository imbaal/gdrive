import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TPasienComponent } from './t-pasien.component';

describe('TPasienComponent', () => {
  let component: TPasienComponent;
  let fixture: ComponentFixture<TPasienComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TPasienComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TPasienComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
