import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { t_Pasien } from 'src/app/models/t_pasien';
import { TPasienService } from 'src/app/services/t_pasien/t-pasien.service';

@Component({
  selector: 'app-t-pasien',
  templateUrl: './t-pasien.component.html',
  styleUrls: ['./t-pasien.component.css']
})
export class TPasienComponent implements OnInit {
  public pasien: any = [];
  public editPasien: t_Pasien;
  public deletePasien: t_Pasien;
  public sortedby = 'fullname';
  public mode = 'asc';
  public search = '';
  public tempSelectID: any = [];
  public currentPage = 1;
  public recordPerPage = 5;
  public totalPage = 0;
  public dobNew : any;

  constructor(private tPasienService: TPasienService) {
    this.editPasien = {} as t_Pasien;
    this.deletePasien = {} as t_Pasien;
  }

  ngOnInit(): void {
    this.getTPasienAPI()
  }

  public getTPasienAPI(): void {
    this.tPasienService.getPasien(this.sortedby, this.mode, this.search, this.currentPage, this.recordPerPage).subscribe(
      (response: any) => {
        this.pasien = response.result;
        this.totalPage = Math.ceil(response.total / response.recordPerPage)
        console.log(this.pasien)
        console.log(this.totalPage)
        console.log(this.currentPage)
        console.log(this.recordPerPage)
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }

  public sortingSortedBy(value: string) {
    this.sortedby = value;
    this.getTPasienAPI();
  }

  public sortingMode(value: string) {
    this.mode = value;
    this.getTPasienAPI();
  }

  public selectRecordPerPage(value: number) {
    this.recordPerPage = value;
    this.getTPasienAPI();
  }

  public onAddPasien(addForm: NgForm): void {
    console.log(addForm.value)
    document.getElementById('add-pasien-form')?.click();

    if (addForm.value.weight == "") {
      addForm.value.weight = 0
    }
    console.log(addForm.value.weight)

    if (addForm.value.height == "") {
      addForm.value.height = 0
    }
    console.log(addForm.value.height)

    this.tPasienService.addPasien(addForm.value).subscribe(
      (response: t_Pasien) => {
        console.log(response);
        this.getTPasienAPI();
        addForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        addForm.reset();
      }
    )
  }

  public onEditPasien(pasien: t_Pasien): void {
    console.log(pasien)
    this.tPasienService.editPasien(pasien).subscribe(
      (response: t_Pasien) => {
        console.log(response);
        this.getTPasienAPI();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }

  public onDeletePasien(id: number): void {
    console.log(id)
    this.tPasienService.deletePasien(id).subscribe(
      () => {
        this.getTPasienAPI();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }

  public iSelected(obj: any, iCheck: boolean) {
    if (iCheck) {
      this.tempSelectID.push({ id: obj.id, fullname: obj.fullname })
    } else {
      let filterArr = [];
      for (let i = 0; i < this.tempSelectID.length; i++) {
        if (this.tempSelectID[i].id !== obj.id) {
          filterArr.push(this.tempSelectID[i])
        }
      }
      this.tempSelectID = filterArr;
    }
    console.log(this.tempSelectID);
  }

  public deleteMulti() {
    for (let i = 0; i < this.tempSelectID.length; i++) {
      this.onDeletePasien(this.tempSelectID[i].id);
    }
    this.tempSelectID = []
  }

  public onSearchPasien(search: any) {
    this.search = search;
    this.getTPasienAPI()
  }

  public nextPage() {
    console.log(this.currentPage)
    console.log(this.totalPage)
    if (this.currentPage < this.totalPage) {
      this.currentPage++;
      this.getTPasienAPI();
    }
  }

  public previousPage() {
    if (this.currentPage > 1) {
      this.currentPage--;
      this.getTPasienAPI();
    }
  }

  public onOpenModal(pasien: t_Pasien, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');

    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');

    if (mode === 'add') {
      button.setAttribute('data-target', '#addTPasienModal');
    }

    if (mode === 'edit') {
      this.editPasien = pasien;
      console.log(this.editPasien)

      let dob_pasien = new Date(this.editPasien.dob);
      let day = dob_pasien.getDate().toString().padStart(2, "0");
      let month = dob_pasien.getMonth().toString().padStart(2, "0");
      let year = dob_pasien.getFullYear();

      this.dobNew = year + '-'  + month + '-' + day

      button.setAttribute('data-target', '#editTPasienModal');
    }

    if (mode === 'delete') {
      this.deletePasien = pasien;
      button.setAttribute('data-target', '#deleteTPasienModal');
    }

    if (mode == 'deleteSelect') {
      button.setAttribute('data-target', '#deleteSelectPasienModal')
    }

    container!.appendChild(button);
    button.click();
  }
}
