import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MUser } from 'src/app/models/m_user';
import { Iqbal } from 'src/app/models/t_customer_wallet';
import { WalletCustomer, DefaultNominal, CustomNominal } from 'src/app/models/t_customer_wallet';
import { LoginService } from 'src/app/services/auth/login.service';
import { TCustomerWalletService } from 'src/app/services/t_customer_wallet/t-customer-wallet.service';
import { TabPembayaranService } from 'src/app/services/t_customer_wallet/tab-pembayaran.service';

@Component({
  selector: 'app-t-profile',
  templateUrl: './t-profile.component.html',
  styleUrls: ['./t-profile.component.css']
})
export class TProfileComponent implements OnInit {
  //start iqbal
  public kartu: any = [];
  public deleteK: Iqbal;
  public tempkartu: any = []
  public tombolaktif: boolean = false
  public jenisbank: any = [];
  public peringatan = ''
  public peringatan2 = false
  public tempvalidasikartu: any = []

  //end iqbal



  // gerin
  public valuewallet: any
  public thrownominal = 0
  public customer_id = 0
  public defaultNominal: any[] = []
  public alphabet = /[a-zA-Z]/
  public pesan = ""
  public hidden: boolean = false
  public buttonhidden: boolean = false
  public buttonhidden1: boolean = false
  public tambahnominal: CustomNominal
  // public addnominal: DefaultNominal
  // end

  public subcription: Subscription;
  public user: MUser = {} as MUser;


  constructor(private walletService: TCustomerWalletService, private kartuService: TabPembayaranService, private loginService: LoginService) {
    this.deleteK = {} as Iqbal
    this.tambahnominal = {} as CustomNominal
    this.valuewallet = {} as WalletCustomer
    this.subcription = this.loginService.currentUser.subscribe( //
      (newUser) => {
        (this.user = newUser)
        console.log(newUser)
      }
    )
  }

  ngOnInit(): void {
    this.onGetKartu();

    this.onGetCustomerId()
  }

  onGetCustomerId() {
    this.walletService.getCusomerId(this.user.id).subscribe(
      (response: any) => {
        this.customer_id = response.result
        this.onGetValueWallet()
      },
      (error: HttpErrorResponse) => {
        alert(error.message)
      }
    )
  }

  public openModal(iden: WalletCustomer, mode: string): void {
    const container = document.getElementById('main-container')
    const button = document.createElement('button')


    button.type = 'button'
    button.style.display = 'none'
    button.setAttribute('data-bs-toggle', 'modal')

    if (mode == 'tambahsaldo') {
      this.onGetNominalDefault()
      this.onGetCustomNominal()
      button.setAttribute('data-bs-target', '#tambahSaldo')
    }
    if (mode == 'succes') {
      button.setAttribute('data-bs-target', '#success')
    }
    container!.appendChild(button)
    button.click()
  }

  public onGetValueWallet() {
    this.walletService.getValueWallet(this.customer_id).subscribe(
      (response: any) => {
        this.valuewallet = response;

      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }
  public setValue(value: any) {
    if (value == "other") {
      this.hidden = true
      this.buttonhidden = true
      this.buttonhidden1 = false

    } else {
      this.thrownominal = parseInt(value)
      this.hidden = false
      this.buttonhidden = false
      this.buttonhidden1 = true
    }
  }

  public onGetWalletTambah() {
    this.walletService.getWalletTambah(this.customer_id, this.thrownominal).subscribe(
      (response: any) => {
        this.onGetValueWallet()
        this.openModal(null!, 'succes')
      },
      (error: HttpErrorResponse) => {
        alert(error.message)
        console.log('error disini')
      }
    )
    this.thrownominal = 0
  }
  public onGetWalletTambah1(addNominal1: NgForm) {
    let batas = 10000
    let count1 = 0
    console.log(addNominal1.value.nominal);
    let addNominal = parseInt(addNominal1.value.nominal)
    console.log(addNominal)
    if (!Number.isInteger(addNominal)) {
      this.pesan = "*Masukan Angka"
    } else {
      if (addNominal >= batas) {
        if (this.customnominal.length == 0) {
          this.thrownominal = addNominal
        } else {
          this.thrownominal = addNominal
          for (let i = 0; i < this.customnominal.length; i++) {
            if (this.thrownominal == this.customnominal[i].nominal) {
              i = this.customnominal.length
              count1++
              this.thrownominal = 0
            }
          }
        }
        if (count1 == 0 && this.thrownominal != 0) {
          this.walletService.getWalletTambah1(this.customer_id, addNominal).subscribe(
            (response: any) => {
              this.onGetValueWallet()
              this.openModal(null!, 'succes')
              this.pesan = ""
              this.hidden = false
            },
            (error: HttpErrorResponse) => {
              alert(error.message)
            }
          )
          count1 = 0
        } else {
          this.hidden = true
          this.pesan = '*data sudah tersimpan coba lagi'
          console.log(this.pesan)
        }
      } else {
        this.hidden = true
        this.pesan = "*Nominal harus minimal 10.000"
        console.log(this.pesan)
      }
      this.thrownominal = 0
    }
  }

  public onGetNominalDefault() {
    this.walletService.getNominalDefault().subscribe(
      (response: any) => {
        this.defaultNominal = response
        console.log(this.defaultNominal)
      },
      (error: HttpErrorResponse) => {
        alert(error.message)
      }
    )
  }

  public customnominal: any[] = []
  public onGetCustomNominal() {
    this.valuewallet.id
    this.walletService.getCustomNominal(this.customer_id).subscribe(
      (response: any) => {
        this.customnominal = response
      },
      (error: HttpErrorResponse) => {
        alert(error.message)
      }
    )
  }


  
  public onGetKartu(): void {
    this.kartuService.getKartu().subscribe(
      (response: any) => {
        console.log(response.result)
        this.tempkartu = ''
        let tempasli = []
        for (let i = 0; i < response.result.length; i++) {
          let temp = ''
          let tempbank = ''
          tempasli[i] = response.result[i].card_number
          let panjang = response.result[i].card_number.length
          for (let j = 0; j < panjang; j++) {

            if (j < panjang - 4) {
              temp += '*'
              if (j < 4) {
                tempbank += response.result[i].card_number.charAt(j)
              }
            } else {
              temp += response.result[i].card_number.charAt(j)
            }
          }
          if (tempbank == '1111') {
            this.jenisbank[i] = 1
          } else if (tempbank == '2222') {
            this.jenisbank[i] = 2
          } else if (tempbank == '3333') {
            this.jenisbank[i] = 3
          } else if (tempbank == '4444') {
            this.jenisbank[i] = 4
          }

          response.result[i].card_number = temp
        }
        this.kartu = response.result;
        this.tempvalidasikartu = tempasli

      }, (error: HttpErrorResponse) => {
        alert(error.message)
      }
    )
  }
  public onGetKartubyid(id:number): void {
    this.kartuService.getKartubyid(id).subscribe(
      (response: any) => {
        console.log(response.result)
        this.tempkartu = ''
        let tempasli = []
        for (let i = 0; i < response.result.length; i++) {
          let temp = ''
          let tempbank = ''
          tempasli[i] = response.result[i].card_number
          let panjang = response.result[i].card_number.length
          for (let j = 0; j < panjang; j++) {

            if (j < panjang - 4) {
              temp += '*'
              if (j < 4) {
                tempbank += response.result[i].card_number.charAt(j)
              }
            } else {
              temp += response.result[i].card_number.charAt(j)
            }
          }
          if (tempbank == '1111') {
            this.jenisbank[i] = 1
          } else if (tempbank == '2222') {
            this.jenisbank[i] = 2
          } else if (tempbank == '3333') {
            this.jenisbank[i] = 3
          } else if (tempbank == '4444') {
            this.jenisbank[i] = 4
          }

          response.result[i].card_number = temp
        }
        this.kartu = response.result;
        this.tempvalidasikartu = tempasli

      }, (error: HttpErrorResponse) => {
        alert(error.message)
      }
    )
  }
  public peringatankartu = false
  public peringatanexp = false
  public peringatanbank = false
  public onAddKartu(addForm: NgForm): void {
    const hariini = new Date()
    let year = hariini.getFullYear()
    let month = hariini.getMonth() + 1 + year * 12
    let sama = 0
    let pembandingbulan = addForm.value.validity_period.split('-')
    let tahun = parseInt(pembandingbulan[0])
    let bulan = parseInt(pembandingbulan[1]) + (tahun * 12)

    if (bulan >= month) {
      this.peringatanexp = false
    } else { this.peringatanexp = true }


    for (let i = 0; i < this.tempvalidasikartu.length; i++) {
      if (addForm.value.card_number == this.tempvalidasikartu[i]) {

        sama++
      }
    }

   

    console.log(bulan, month)


    let temp = ''
    for (let i = 0; i < 4; i++) {
      const text: string = addForm.value.card_number.toString()
      temp += text.charAt(i)
    }
  
    if (sama < 1) {
      this.peringatankartu = false
      if (temp == "1111" || temp == '2222' || temp == '3333' || temp == '4444') {
        console.log(temp)
        this.kartuService.addKartu(addForm.value).subscribe(
          (response: Iqbal) => {
            this.onGetKartu();
            addForm.reset();
          }, (error: HttpErrorResponse) => {
            alert(error.message);
            addForm.reset();
          }
        )
      }
      else { this.peringatanbank = true}
    }
    else { this.peringatankartu = true }
    



    //} 
    console.log(this.peringatanexp);
    console.log(this.peringatankartu);
    console.log(this.peringatanbank);
  }


  public onDeleteKartu(id: number): void {
    console.log(id)
    this.kartuService.deleteKartu(id).subscribe(
      (response: any) => {
        this.kartuService;
        this.onGetKartu();
      },
      (error: HttpErrorResponse) => {
        alert(error.message)
      }
    )
  }

  public onOpenModal(c: Iqbal, mode: string): void {
    const container = document.getElementById('main-container')
    const button = document.createElement('button')

    button.type = 'button'
    button.style.display = 'none'
    button.setAttribute('data-bs-toggle', 'modal')

    if (mode == 'add') {
      button.setAttribute('data-bs-target', '#addKartuModal')
    }
    if (mode == 'delete') {
      this.deleteK = c;
      console.log(this.deleteK)
      button.setAttribute('data-bs-target', '#DeleteKartuPembayaran')
    }
    if (mode == 'aktif') {
      this.tombolaktif = true;
      button.setAttribute('data-bs-target', '#AktifkanDompet')
    }
    if (mode == 'tutup') {
      this.tombolaktif = false;
    }
    if (mode == 'bhapus') {
      button.setAttribute('data-bs-target', '#Berhasildihapus')
    }
    if (mode == 'cari') {
      button.setAttribute('data-bs-target', '#CariObat')
    }
    container!.appendChild(button)
    button.click()
  }

}