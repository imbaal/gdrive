import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { MProfileDoctor } from 'src/app/models/m_profile_doctor';
import { MUser } from 'src/app/models/m_user';
import { LoginService } from 'src/app/services/auth/login.service';
import { ProfileDoctorService } from 'src/app/services/m_profile_doctor/profile-doctor.service';
import { TCurrentDoctorSpecializationService } from 'src/app/services/t_current_doctor_specialization/t-current-doctor-specialization.service';
import { DetailDoctorService } from 'src/app/services/t_detail_doctor/detail-doctor.service';

@Component({
  selector: 'app-detail-doctor',
  templateUrl: './detail-doctor.component.html',
  styleUrls: ['./detail-doctor.component.css']
})
export class DetailDoctorComponent implements OnInit {

  public mProfileDoctor: MProfileDoctor = {} as MProfileDoctor
  public MDoctorTreatment: any = [];
  public MDoctorEducation: any = [];
  public MRiwayatPraktek: any = [];
  public TLokasiPraktek: any = [];
  public TJadwalPraktek: any = [];

  public imgUrl: SafeUrl | null = null

  public subcription: Subscription;
  public user: MUser = {} as MUser;

  public biodata_id = 0;
  public id = 0;
  public medical_facility_id = 0;

  public selectedType = 'opentype';

  constructor(
    private sanitizer: DomSanitizer,
    private activatedRoute: ActivatedRoute,
    private HttpClient: HttpClient,
    private MProfileDoctorService: ProfileDoctorService,
    private TSpecializationService: TCurrentDoctorSpecializationService,
    private TDetailDoctorService: DetailDoctorService,
    private LoginService: LoginService,) {

    this.subcription = this.LoginService.currentUser.subscribe( //
      (newUser) => {
        (this.user = newUser)
      }
    )
  }

  ngOnInit(): void {
    this.getMProfileDoctorByIDAPI()
    console.log(this.user);
    
  }

  onJadwalLokasi(event: any) {
    this.selectedType = event.target.value;
  }

  public getMProfileDoctorByIDAPI(): void {
    this.MProfileDoctorService.getMProfileDoctorByID(this.user.biodata_id).subscribe(
      (response: any) => {
        this.mProfileDoctor = response.result;
        const image = this.mProfileDoctor.image
        console.log(this.mProfileDoctor);
        this.id = this.mProfileDoctor.id

        this.getMDoctorTreatmentByIDAPI(this.id);
        this.getMDoctorEducationByIDAPI(this.id);
        this.getMRiwayatPraktekByIDAPI(this.id);
        this.getPraktekDoctorAPI(this.id)

        fetch('data:image/png;base64,' + image).then(res => res.blob()).then(blob => {
          const file = new Blob([blob], { type: 'application/image' })
          const unsafeImg = URL.createObjectURL(file)
          this.imgUrl = this.sanitizer.bypassSecurityTrustUrl(unsafeImg)
        })
     
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public getMDoctorTreatmentByIDAPI(id: number): void {
    this.MProfileDoctorService.getMDoctorTreatmentByID(this.id).subscribe(
      (response: any) => {
        this.MDoctorTreatment = response.result;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public getMDoctorEducationByIDAPI(id: number): void {
    this.MProfileDoctorService.getMDoctorEducationByID(this.id).subscribe(
      (response: any) => {
        this.MDoctorEducation = response.result;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public getMRiwayatPraktekByIDAPI(id: number): void {
    this.MProfileDoctorService.getMRiwayatPraktekByID(this.id).subscribe(
      (response: any) => {
        this.MRiwayatPraktek = response.result;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public getPraktekDoctorAPI(id:number): void {
    this.TDetailDoctorService.getPraktekDoctor(id).subscribe(
      (response: any) => {
        this.TLokasiPraktek = response.result;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  // public getJadwalPraktekDoctorAPI(id: number): void {
  //   this.TDetailDoctorService.getLokasiPraktekDoctor(id).subscribe(
  //     (response: any) => {
  //       this.TJadwalPraktek = response.result;
  //     },
  //     (error: HttpErrorResponse) => {
  //       alert(error.message);
  //     }
  //   );
  // }

}
