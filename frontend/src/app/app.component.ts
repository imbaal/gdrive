import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { MUser } from './models/m_user';
import { LoginService } from './services/auth/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public subcription: Subscription;
  public user: MUser = {} as MUser;

  constructor(private route: Router, private loginService: LoginService) {
    this.subcription = this.loginService.currentUser.subscribe(
      (newUser) => {
        (this.user = newUser)
        // console.log(newUser)
      }
    )
  }

  title = 'frontend';
}
