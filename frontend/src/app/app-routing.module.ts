import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { MBankComponent } from './pages/m-bank/m-bank.component';
import { MBloodGroupComponent } from './pages/m-blood-group/m-blood-group.component';
import { MPaymentComponent } from './pages/m-payment/m-payment.component';
import { MSpecializationComponent } from './pages/m-specialization/m-specialization.component';
import { TProfileComponent } from './pages/t-profile/t-profile.component';
import { TPasienComponent } from './pages/t-pasien/t-pasien.component';
import { TCariobatComponent } from './pages/t-cariobat/t-cariobat.component';
import { MWalletDefaultNominalComponent } from './pages/m-wallet-default-nominal/m-wallet-default-nominal.component';
import { ProfileDoctorComponent } from './pages/profile-doctor/profile-doctor.component';
import { DetailDoctorComponent } from './pages/detail-doctor/detail-doctor.component';
import { TCariDokterComponent } from './pages/t-cari-dokter/t-cari-dokter.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'm-specialization', component: MSpecializationComponent },
  { path: 'Bank', component: MBankComponent },
  { path: 'payment', component: MPaymentComponent },
  { path: 'mbloodgroup', component: MBloodGroupComponent },
  { path: 'profile', component: TProfileComponent },
  { path: 'bank', component: MBankComponent },
  { path: 'payment', component: MPaymentComponent },
  { path: 'pasien', component: TPasienComponent },
  { path: 'cariobat', component: TCariobatComponent },
  { path: 'defaultnominal', component: MWalletDefaultNominalComponent },
  { path: 'profile-doctor', component: ProfileDoctorComponent },
  { path: 'detail-doctor', component: DetailDoctorComponent },
  { path: 'cari-dokter', component: TCariDokterComponent },


];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled',
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
