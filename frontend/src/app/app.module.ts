import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { MSpecializationComponent } from './pages/m-specialization/m-specialization.component';
import { HomeComponent } from './pages/home/home.component';

import { MBankComponent } from './pages/m-bank/m-bank.component';

import { MSpecializationService } from './services/m_specialization/m-specialization.service';
import { HttpClientModule } from '@angular/common/http';
import { MBankService } from './services/m_bank/m-bank.service';
import { MPaymentComponent } from './pages/m-payment/m-payment.component';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { MBloodGroupComponent } from './pages/m-blood-group/m-blood-group.component';
import { TPasienComponent } from './pages/t-pasien/t-pasien.component';
import { TProfileComponent } from './pages/t-profile/t-profile.component';
import { NavComponent } from './components/nav/nav.component';
import { TCariobatComponent } from './pages/t-cariobat/t-cariobat.component';
import { TCustomerWalletService } from './services/t_customer_wallet/t-customer-wallet.service';
import { MWalletDefaultNominalComponent } from './pages/m-wallet-default-nominal/m-wallet-default-nominal.component';

import { SidebarComponent } from './components/sidebar/sidebar.component';
import { TPasienService } from './services/t_pasien/t-pasien.service';
import { ProfileDoctorComponent } from './pages/profile-doctor/profile-doctor.component';
import { DetailDoctorComponent } from './pages/detail-doctor/detail-doctor.component';
import { TCurrentDoctorSpecializationService } from './services/t_current_doctor_specialization/t-current-doctor-specialization.service';
import { ProfileDoctorService } from './services/m_profile_doctor/profile-doctor.service';
import { FooterComponent } from './components/footer/footer.component';
import { TCariDokterComponent } from './pages/t-cari-dokter/t-cari-dokter.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    MSpecializationComponent,
    HomeComponent,
    MBankComponent,
    MPaymentComponent,
    MBloodGroupComponent,
    TProfileComponent,
    TPasienComponent,
    NavComponent,
    SidebarComponent,
    FooterComponent,
    TCariobatComponent,
    MWalletDefaultNominalComponent,
    NavComponent,
    SidebarComponent,
    ProfileDoctorComponent,
    DetailDoctorComponent,
    TCariDokterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,

    HttpClientModule,
    FormsModule,
    NgxPaginationModule,
  ],
  providers: [MSpecializationService, MBankService, TCurrentDoctorSpecializationService, ProfileDoctorService,TPasienService],
  bootstrap: [AppComponent]
})
export class AppModule { }


