const { request, response } = require("express")

module.exports = exports = (app, pool2) => {
    app.get('/api/bank', (request, response) => {
        const query = `SELECT * FROM m_bank WHERE is_delete = FALSE ORDER BY name`

        pool2.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows,
                });
            }
        });
    });

    app.get('/api/bank/:search', (request, response) => {
        let search = request.params.search
        const query = `SELECT * FROM m_bank
            WHERE LOWER (name) LIKE LOWER
            ('%${search}%') AND is_delete = false
            ORDER BY name`;

        pool2.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error
                });
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows
                });
            }
        });
    })

    app.get('/api/banksorting/:mode', (request, response) => {
        const mode = request.params.mode
        const query = `SELECT * FROM m_bank WHERE is_delete = false ORDER BY name ${mode}`;

        pool2.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows,
                });
            }
        });
    })

    app.post('/api/addbank', (request, response) => {
        const { name, va_code } = request.body;
        const query = `INSERT INTO public.m_bank(
            name, va_code, created_by, created_on)
            VALUES ('${name}', '${va_code}', '1', 'now()')`;

        pool2.query(query, (error, result) => {

            console.log(result);

            if (error) {
                response.send(400, {
                    success: false,
                    result: error
                });
            } else {
                response.send(200, {
                    success: true,
                    result: `Data Bank ${name} saved`
                });
            }
        });
    });

    app.put('/api/updatebank/:id', (request, response) => {
        const { name, va_code } = request.body;
        const ids = request.params.id;
        const query = `UPDATE public.m_bank SET name = 
            '${name}',va_code = ${va_code}, modified_by = 2, modified_on = now() WHERE id = '${ids}'`

        pool2.query(query, (error, result) => {

            console.log(result);

            if (error) {
                response.send(400, {
                    success: false,
                    result: error
                });
            } else {
                response.send(200, {
                    success: true,
                    result: `Data Bank ${name} change`
                });
            }
        });
    })

    app.put('/api/deletebank/:id', (request, response) => {
        const ids = request.params.id;
        const query = `UPDATE public.m_bank SET is_delete = 'true', modified_by = 2, modified_on = now() WHERE id = '${ids}'`

        pool2.query(query, (error, result) => {

            console.log(result);

            if (error) {
                response.send(400, {
                    success: false,
                    result: error
                });
            } else {
                response.send(200, {
                    success: true,
                    result: `Data '${ids}' delete`
                });
            }
        });
    })
}