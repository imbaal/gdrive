

module.exports = exports = (app, db) => {
    app.get('/api/payment_method', (request, response) => {
        const query = 'SELECT *FROM m_payment_method WHERE is_delete = true'
        db.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    status: false,
                    result: error
                })
            } else {
                response.send(200, {
                    status: true,
                    result: result.rows
                })
                console.log(result)
            }
      
        })
    })

    app.post('/api/payment_method/add/:name', (request, response) => {
        const name  = request.params.name
        db.query(`INSERT INTO m_payment_method
         (name, created_by,created_on,is_delete) 
         VALUES ('${name}',1,now(),true)`,
            (error, result) => {
                if (error) {
                    response.send(400, {
                        status: false,
                        result: error
                    })
                } else {
                    response.send(200, {
                        status: true,
                        result: 'Data berhasil ditambah'
                    })
                }
            })
    })

    app.put('/api/payment_method/update/:id', (request, response) => {
        const id = request.params.id
        const { name } = request.body;
        db.query(`UPDATE m_payment_method SET  name = '${name}', modified_by=1, modified_on=now() WHERE id=${id}`,
            (error, result) => {
                if (error) {
                    response.send(400, {
                        status: false,
                        result: error
                    })
                } else {
                    response.send(200, {
                        status: true,
                        result: 'Data berhasil diUpdate',
                        name: name
                    })
                }
            })
    })

    app.put('/api/payment_method/delete/:id', (request, response) => {
        const id = request.params.id
       
        db.query(`UPDATE m_payment_method SET  modified_by=1, modified_on=now(),is_delete=false WHERE id=${id}`,
            (error, result) => {
                if (error) {
                    response.send(400, {
                        status: false,
                        result: error
                    })
                } else {
                    response.send(200, {
                        status: true,
                        result: 'Data berhasil diHapus',
                    })
                }
            })
    })
    app.get('/api/payment_method/search/:name', (request, response) => {
        const name = request.params.name    
        let query=`SELECT*FROM  m_payment_method WHERE is_delete=TRUE AND LOWER(name) LIKE LOWER('%${name}%')`
        db.query( query,
        (error, result) => {
            if (error) {
                response.send(400, {
                    status: false,
                    result: error
                })
            } else {
                response.send(200, {
                    status: true,
                    result: result.rows,
                    names: name,
                    total: result.CountRows
                })
            }
            console.log(result.rows)
        })
    })

    app.get('/api/sorted/:sortBy', (req, res) => {
        const sortBy = req.params.sortBy
        const query = `SELECT * FROM  m_payment_method WHERE is_delete=true ORDER BY ${sortBy}`
        db.query(query, (error, result) => {
          if (error) {
            res.send(400, {
              status: false,
              result: error
            })
          } else {
            res.send(200, {
              status: true,
              result: result.rows
            })
          }
        })
      })
      app.get('/api/filtered/:sort/:mode', (req, res) => {
        const sort = req.params.sort
        const mode = req.params.mode
        const query = `SELECT * FROM  m_payment_method WHERE is_delete=true ORDER BY ${sort} ${mode}`
        db.query(query, (error, result) => {
          if (error) {
            res.send(400, {
              status: false,
              result: error
            })
          } else {
            res.send(200, {
              status: true,
              result: result.rows
            })
          }
        })
      })
  
}