const { request, response } = require("express");
const res = require("express/lib/response");

module.exports = exports = (app, pool3) => {

    app.get('/api/lokasi_praktek/:id', (request, response) => {
        const id = request.params.id;
        // id = 1
        const query = `SELECT Md.id, Mb.fullname, mmf.id AS medical_facility_id, Mmfc.name AS category, Mmf.name AS faskes_name, 
        Tdo.specialization, Ml.name AS location, Mmf.full_address, Mmfs.id AS jadwal_id,
                Mmfs.day, Mmfs.time_schedule_start, Mmfs.time_schedule_end
                FROM public.m_doctor Md
                JOIN public.m_biodata Mb ON Md.biodata_id = Mb.id
                JOIN public.t_doctor_office Tdo ON Tdo.doctor_id = Md.id
                JOIN public.m_medical_facility Mmf ON Mmf.id = Tdo.medical_facility_id
                JOIN public.m_medical_facility_category Mmfc ON Mmfc.id = Mmf.medical_facility_category_id
                JOIN public.m_location Ml ON Ml.id = Mmf.location_id
                JOIN public.m_medical_facility_schedule Mmfs ON Mmfs.medical_facility_id = Mmf.id
                WHERE Md.id = ${id} AND Tdo.is_delete = FALSE`

        pool3.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows,
                });
                // newDoctor = result.rows
                // newLokasi = []
                // newJadwal = []
                // for (let i = 0; i < newDoctor.length; i++) {
                //     if (i < newDoctor.length - 1) {
                //         if (newDoctor[i].medical_facility_id == newDoctor[i + 1].medical_facility_id) {
                //             newLokasi.push({
                //                 faskes_name: newDoctor[i].faskes_name,
                //                 category: newDoctor[i].category,
                //                 full_address: newDoctor[i].full_address
                //             })

                //         } else {
                //             newLokasi.push({
                //                 faskes_name: newDoctor[i].faskes_name,
                //                 category: newDoctor[i].category,
                //                 full_address: newDoctor[i].full_address
                //             })

                //             newJadwal.push({
                //                 medical_facility_id: newDoctor[i].medical_facility_id,
                //                 jadwal_id: newDoctor[i].jadwal_id,
                //                 day: newDoctor[i].day,
                //                 time_schedule_start: newDoctor[i].time_schedule_start,
                //                 time_schedule_end: newDoctor[i].time_schedule_end
                //             })
                //         }
                //     } else {
                //         newLokasi.push({
                //             faskes_name: newDoctor[i].faskes_name,
                //             category: newDoctor[i].category,
                //             full_address: newDoctor[i].full_address
                //         })

                //         newJadwal.push({
                //             medical_facility_id: newDoctor[i].medical_facility_id,
                //             jadwal_id: newDoctor[i].jadwal_id,
                //             day: newDoctor[i].day,
                //             time_schedule_start: newDoctor[i].time_schedule_start,
                //             time_schedule_end: newDoctor[i].time_schedule_end
                //         })
                //     }
                // }
            }
        })
    })

    app.get('/api/lokasi_praktek1/:id', (req, res) => {
        const id=req.params.id
        let query = `SELECT Md.id, Mb.fullname, mmf.id AS medical_facility_id, Mmfc.name AS category, Mmf.name AS faskes_name, Mmf.id as idrs,
        Tdo.specialization, Ml.name AS location, Mmf.full_address
                FROM public.m_doctor Md
                JOIN public.m_biodata Mb ON Md.biodata_id = Mb.id
                JOIN public.t_doctor_office Tdo ON Tdo.doctor_id = Md.id
                JOIN public.m_medical_facility Mmf ON Mmf.id = Tdo.medical_facility_id
                JOIN public.m_medical_facility_category Mmfc ON Mmfc.id = Mmf.medical_facility_category_id
                JOIN public.m_location Ml ON Ml.id = Mmf.location_id
                WHERE Tdo.is_delete = FALSE and md.id=${id}
                ORDER BY md.id asc`

        pool3.query(query, (err, result) => {
            if (err) {
                res.send(400, {
                    status: false
                })
            } else {
                doctor = result.rows
                newDoctor = []
                newRumahSakit = []
                for (i = 0; i < doctor.length; i++) {
                    if (i < doctor.length - 1) {
                        if (doctor[i].id == doctor[i + 1].id) {
                            newRumahSakit.push({
                                idrs: doctor[i].idrs,
                                faskes_name: doctor[i].faskes_name,
                                category: doctor[i].category,
                                full_address: doctor[i].full_address
                            })

                        } else {
                            newRumahSakit.push({
                                idrs: doctor[i].idrs,
                                faskes_name: doctor[i].faskes_name,
                                category: doctor[i].category,
                                full_address: doctor[i].full_address
                            })
                            newDoctor.push({
                                id: doctor[i].id,
                                rumahsakit: newRumahSakit
                            })
                            newRumahSakit = []
                        }
                    } else {
                        newRumahSakit.push({
                            idrs: doctor[i].idrs,
                            faskes_name: doctor[i].faskes_name,
                            category: doctor[i].category,
                            full_address: doctor[i].full_address
                        })
                        newDoctor.push({
                            id: doctor[i].id,
                            rumahsakit: newRumahSakit
                        })
                        newRumahSakit = []
                    }
                }

                query = `SELECT md.id, mmf.id as idrs, Mmfs.id AS jadwal_id,Mmfs.day, Mmfs.time_schedule_start, Mmfs.time_schedule_end
                From m_doctor md
                Join t_doctor_office tdo
                on tdo.doctor_id=md.id
                JOIN m_medical_facility mmf
                ON mmf.id = Tdo.medical_facility_id
                JOIN public.m_medical_facility_schedule Mmfs 
                ON Mmfs.medical_facility_id = Mmf.id
                WHERE md.id=${id}`
                

                pool3.query(query, (err, result) => {
                    if (err) {
                        res.send(400, {
                            status: false
                        })
                    } else {
                        jadwal = result.rows
                        newJadwal = []
                        finalDoctor = []
                        finaljadwal=[]
                        for (i = 0; i < newDoctor.length; i++) {//1
                            rumahsakit = newDoctor[i].rumahsakit
                            console.log(rumahsakit.length, 'jumlah rs');

                            for (j = 0; j < rumahsakit.length; j++) {//idrs=1 idrs1
                                for (k = 0; k < jadwal.length; k++) {//1133
                                    if (rumahsakit[j].idrs == jadwal[k].idrs && newDoctor[i].id==jadwal[k].id) {//1==1
                                        newJadwal.push({
                                            day: jadwal[k].day,
                                            time_schedule_start: jadwal[k].time_schedule_start,
                                            time_schedule_end: jadwal[k].time_schedule_end,
                                        })
                                    } 
                                    
                                }
                                finaljadwal.push(
                                     { ...rumahsakit[j], newJadwal }
                                )
                                newJadwal=[]
                            }
                           finalDoctor.push({
                               id:newDoctor[i].id,
                               finaljadwal
                           })
                           finaljadwal=[]
                           newJadwal=[]
                        }
                    }

                    res.send(200, {
                    
                        result:finalDoctor
                    })

                })
            }
        })
    })
}
