const { request, response } = require("express")

module.exports = exports = (app, pool2) => {
    app.get('/api/pasien/', (request, response) => {
        let sortedby = request.query.sortedby;
        let mode = request.query.mode;
        let search = request.query.search;
        let qPage = request.query.currentPage
        let qPer_Page = request.query.recordPerPage
    
        let recordPerPage = parseInt(qPer_Page)
        let currentPage = parseInt(qPage)

        search = search === undefined ?  '' : search
        sortedby = sortedby === undefined ? 'fullname' : sortedby
        mode = mode === undefined ? 'asc' : mode
        recordPerPage = qPer_Page === undefined ? 5 : qPer_Page
        console.log(recordPerPage)
        currentPage = qPage === undefined ? 0 : (currentPage - 1) * recordPerPage
    
        let pages = qPage === undefined ? 1 : qPage
    
        const query = `SELECT mc.id, mb.fullname, mc.dob, mc.gender, mc.blood_group_id, mc.rhesus_type, mc.weight,
            mc.height, mcr.id as customer_relation_id, mcr.name as relasi, DATE_PART('year', AGE(mc.dob)) AS usia
            FROM m_customer mc
            JOIN m_biodata mb
            ON mc.biodata_id = mb.id
            JOIN m_customer_member mcm
            ON mcm.customer_id = mc.id
            LEFT JOIN m_customer_relation mcr
            ON mcr.id = mcm.customer_relation_id
            WHERE LOWER (mb.fullname)
            LIKE LOWER ('%${search}%') AND mc.is_delete = 'false'
            ORDER BY ${sortedby} ${mode}
            LIMIT ${recordPerPage} OFFSET ${currentPage}`;
    
        const query2 = `SELECT mc.id, mb.fullname, mc.dob, mc.gender, mc.blood_group_id, mc.rhesus_type, mc.weight,
        mc.height, mcr.id as customer_relation_id, mcr.name as relasi, DATE_PART('year', AGE(mc.dob)) AS usia
        FROM m_customer mc
        JOIN m_biodata mb
        ON mc.biodata_id = mb.id
        JOIN m_customer_member mcm
        ON mcm.customer_id = mc.id
        LEFT JOIN m_customer_relation mcr
        ON mcr.id = mcm.customer_relation_id
        WHERE LOWER (mb.fullname)
        LIKE LOWER ('%${search}%') AND mc.is_delete = 'false'`;
    
        pool2.query(query2, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                let totalItems = result.rowCount;
                pool2.query(query, (error2, result2) => {
                    if (error2) {
                        response.send(400, {
                            succsess: false,
                            result: error2,
                        });
                    } else {
                        response.send(200, {
                            currentPage: parseInt(pages),
                            recordPerPage: parseInt(recordPerPage),
                            total: totalItems,
                            result: result2.rows,
                            succsess: true,
                        });
                    }
                });
            }
        });
    });
    // app.get('/api/pasien', (request, response) => {
        //     const query = `SELECT mc.id, mb.fullname, mc.dob, mc.gender, mc.blood_group_id, mc.rhesus_type, mc.weight, mc.height,
        //         mcr.name as relasi, DATE_PART('year', AGE(mc.dob)) AS usia
        //         FROM m_customer mc
        //         JOIN m_biodata mb
        //         ON mc.biodata_id = mb.id
        //         JOIN m_customer_member mcm
        //         ON mcm.customer_id = mc.id
    //         LEFT JOIN m_customer_relation mcr
    //         ON mcr.id = mcm.customer_relation_id
    //         WHERE mc.is_delete = 'false'
    //         ORDER BY mb.fullname`;

    //     pool2.query(query, (error, result) => {
    //         if (error) {
    //             response.send(400, {
    //                 success: false,
    //                 result: error,
    //             });
    //         } else {
    //             response.send(200, {
    //                 success: true,
    //                 result: result.rows,
    //             });
    //         }
    //     });
    // });

    app.get('/api/pasien/:id', (request, response) => {
        const ids = request.params.id;
        const query = `SELECT mc.id, mb.fullname, mc.dob, mc.gender, mc.blood_group_id, mc.rhesus_type, mc.weight, mc.height,
            mcr.id as customer_relation_id, mcr.name as relasi, DATE_PART('year', AGE(mc.dob)) AS usia
            FROM m_customer mc
            JOIN m_biodata mb
            ON mc.biodata_id = mb.id
            JOIN m_customer_member mcm
            ON mcm.customer_id = mc.id
            LEFT JOIN m_customer_relation mcr
            ON mcr.id = mcm.customer_relation_id
            WHERE id = '${ids}' AND mc.is_delete = 'false'
            ORDER BY mb.fullname`;

        pool2.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows[0],
                });
            }
        });
    });


    // app.get('/api/pasien/:search', (request, response) => {
    //     let search = request.params.search
    //     const query = `SELECT mc.id, mb.fullname, mc.dob, mc.gender, mc.blood_group_id, mc.rhesus_type, mc.weight,
    //     mc.height, mcr.name as relasi, DATE_PART('year', AGE(mc.dob)) AS usia
    //     FROM m_customer mc
    //     JOIN m_biodata mb
    //     ON mc.biodata_id = mb.id
    //     JOIN m_customer_member mcm
    //     ON mcm.customer_id = mc.id
    //     LEFT JOIN m_customer_relation mcr
    //     ON mcr.id = mcm.customer_relation_id
    //     WHERE LOWER (mb.fullname)
    //     LIKE LOWER ('%${search}%') AND mc.is_delete = 'false'`;

    //     pool2.query(query, (error, result) => {
    //         if (error) {
    //             response.send(400, {
    //                 success: false,
    //                 result: error
    //             });
    //         } else {
    //             response.send(200, {
    //                 success: true,
    //                 result: result.rows
    //             });
    //         }
    //     });
    // })

    app.post('/api/addpasien', (request, response) => {
        const { fullname, dob, gender, blood_group_id, rhesus_type, height, weight, customer_relation_id } = request.body;

        pool2.query(`INSERT INTO public.m_biodata(
            fullname, created_by, created_on, is_delete)
            VALUES ('${fullname}', 1, 'now()', 'false')`, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error
                });
            } else {
                pool2.query(`SELECT id FROM m_biodata WHERE is_delete = 'false' AND fullname = '${fullname}'
                            ORDER BY id DESC LIMIT 1`, (error2, result2) => {
                    if (error2) {
                        response.send(400, {
                            success: false,
                            result: error2
                        });
                    } else {
                        pool2.query(`INSERT INTO public.m_customer(
                            biodata_id, dob, gender, blood_group_id, rhesus_type, height, weight, created_by, created_on, is_delete)
                            VALUES ('${result2.rows[0].id}', '${dob}', '${gender}', ${blood_group_id}, '${rhesus_type}', '${height}',
                                    '${weight}', 1, 'now()', 'false')`, (error3, result3) => {
                            if (error3) {
                                response.send(400, {
                                    success: false,
                                    result: error3
                                });
                            } else {
                                pool2.query(`INSERT INTO public.m_customer_member(
                                    parent_biodata_id, customer_id, customer_relation_id, created_by, created_on, is_delete)
                                    VALUES (1, '${result2.rows[0].id}', '${customer_relation_id}', 1, 'now()', 'false')`,
                                    (error4, result4) => {
                                        if (error4) {
                                            response.send(400, {
                                                success: false,
                                                result: error4
                                            });
                                        } else {
                                            pool2.query(`SELECT mc.id, mb.fullname, mb.id as biodata_id, mcr.id as customer_relation_id,
                                            mcr.name as relasi, mc.dob,
                                            DATE_PART('year', AGE(mc.dob)) AS usia, mc.gender, mc.rhesus_type, mc.height, mc.weight
                                            FROM m_customer mc
                                            JOIN m_biodata mb
                                            ON mc.biodata_id = mb.id
                                            JOIN m_customer_member mcm
                                            ON mcm.customer_id = mc.id
                                            LEFT JOIN m_customer_relation mcr
                                            ON mcr.id = mcm.customer_relation_id
                                            WHERE mb.fullname = '${fullname}' AND mc.is_delete = 'false'`, (error5, result5) => {
                                                if (error5) {
                                                    response.send(400, {
                                                        success: false,
                                                        result: error5
                                                    });
                                                } else {
                                                    response.send(200, {
                                                        success: true,
                                                        result: result5.rows
                                                    });
                                                }
                                            });
                                        }
                                    });
                            }
                        })
                    }
                });
            }
        });
    });

    app.put('/api/updatepasien/:id', (request, response) => {
        const { fullname, dob, gender, blood_group_id, rhesus_type, height, weight, customer_relation_id } = request.body;
        const ids = request.params.id;

        pool2.query(`SELECT * FROM m_customer WHERE id = ${ids}`, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    response: "slaah",
                    result: error
                });
            } else {
                const id_biodata = result.rows[0].biodata_id;
                pool2.query(`UPDATE m_biodata SET fullname = '${fullname}' WHERE id = ${id_biodata}`, (error2, result2) => {
                    if (error2) {
                        response.send(400, {
                            success: false,
                            response: "salah2",
                            result: error2
                        });
                    } else {
                        pool2.query(`UPDATE m_customer SET dob = '${dob}', gender = '${gender}',
                        blood_group_id = ${blood_group_id}, rhesus_type = '${rhesus_type}', height = '${height}',
                        weight = '${weight}' WHERE id = ${ids}`,
                            (error3, result3) => {
                                if (error3) {
                                    response.send(400, {
                                        success: false,
                                        response: "salah3",
                                        result: error3
                                    });
                                } else {
                                    pool2.query(`UPDATE m_customer_member SET customer_relation_id = ${customer_relation_id}
                                WHERE id = '${ids}'`,
                                        (error4, result4) => {
                                            if (error4) {
                                                response.send(400, {
                                                    success: false,
                                                    response: "salah4",
                                                    result: error4
                                                });
                                            } else {
                                                response.send(200, {
                                                    success: true,
                                                    result: `Data Peserta ${fullname} change`
                                                });
                                            }
                                        });
                                }
                            });
                    }
                });
            }
        });
    })

    app.put('/api/deletepasien/:id', (request, response) => {
        const ids = request.params.id;
        const query = `UPDATE public.m_customer SET is_delete = 'true', modified_by = 2, modified_on = 'now()' WHERE id = '${ids}'`

        pool2.query(query, (error, result) => {

            console.log(result);

            if (error) {
                response.send(400, {
                    success: false,
                    result: error
                });
            } else {
                response.send(200, {
                    success: true,
                    result: `Data '${ids}' delete`
                });
            }
        });
    })
}