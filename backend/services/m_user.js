const {
    request,
    response,
    query
} = require("express")
const nodemailer = require('nodemailer');
const { error } = require("server/router");
// const bcrypt = require('bcrypt')
// const jwt = require("jsonwebtoken")

let transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true, // true for 465, false for other ports
    service: 'Gmail',

    auth: {
        user: 'mrxx038@gmail.com', // generated ethereal user
        pass: 'mrx13579', // generated ethereal password
    },
});

let codeOtp;

module.exports = exports = (app, pool3) => {

    /*----- GET ALL USER -----*/

    app.get('/api/auth/user/all', (request, response) => {

        const query = `SELECT * FROM m_user WHERE is_delete = false`

        pool3.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error
                })
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    })

    /*----- REGISTRASI -----*/
    app.post("/api/auth/registrasi", (request, response) => {
        const {
            email
        } = request.body;

        const query = `SELECT mu.email FROM m_user mu WHERE email = '${email}'`

        pool3.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error
                })
            } else {
                if (result.rowCount < 1) {

                    // OTP CODE
                    let otp = Math.random();
                    otp = otp * 10000;
                    otp = parseInt(otp);
                    console.log(otp);
                    codeOtp = otp;

                    let mailOptions = {
                        to: request.body.email,
                        subject: "Otp for registration is: ",
                        html: "<h3>OTP for account verification is </h3>" + "<h1 style='font-weight:bold;'>" + otp + "</h1>" // html body
                    };

                    transporter.sendMail(mailOptions, (error, info) => {
                        if (error) {
                            return console.log(error);
                        }
                        console.log('Message sent: %s', info.messageId);
                        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
                        response.render('otp');
                    });

                    let timestamp = Date.now()/1000;

                    const query = `INSERT INTO t_token(email, token, expired_on, is_expired, used_for, created_by, created_on, is_delete
                        ) VALUES ('${email}', '${codeOtp}', '${timestamp} + 600000', false, 'Register', '1', 'now()', false)`

                    pool3.query(query, (error, result) => {
                        if (error) {
                            response.send(400, {
                                success: false,
                                result: `Error insert data ke tabel t_token`
                            })
                            console.log(`fbgnh`);
                        } else {
                            response.send(200, {
                                success: true,
                                result: `Email dapat digunakan dan sudah menerima token`
                            })
                        }
                    })
                } else {
                    response.send(401, {
                        success: false,
                        result: `Email sudah terpakai`
                    })
                    console.log(result);
                }
            }
            // console.log(result);
        })
    })

    app.post('/api/auth/resendOtp', (request, response) => {})

    app.post('/api/auth/verifyOtp', (request, response) => {
        const { kodeOtp } = request.body;

        if (kodeOtp == codeOtp) {
            response.send(200, {
                success: true,
                result: "Kode OTP benar"
            })
        } else {
            response.send(400, {
                success: false,
                result: "Kode OTP salah"
            })
        }
    });

    /*----- DAFTAR AKUN -----*/

    app.post("/api/auth/daftarAkun", (request, response) => {
        const { fullname, mobile_phone, role_id, email, password } = request.body;

        const query = `SELECT id FROM m_user ORDER BY id DESC LIMIT 1`

        pool3.query(query, (error, result) => {
            let id = 0;
            if(result.rowCount > 0){
                id = parseInt(result.rows[0].id) + 1
            } else {
                id = 1
            }
            console.log(id);

            if (error) {
                response.send(400, {
                    success: false,
                    result: error
                })
            } else {
                const query = `INSERT INTO m_biodata ( fullname, mobile_phone, created_by, created_on, is_delete
                        ) VALUES ( '${fullname}', '+62${mobile_phone}', '${id}', now(), false)`

                pool3.query(query, (error, result) => {
                    if(error) {
                        response.send(400, {
                            success: false,
                            result: `Error insert data ke m_biodata`
                        })
                    } else {
                        const query = `SELECT id from m_biodata ORDER BY id DESC LIMIT 1`

                        pool3.query(query, (error, result) => {
                            let idbio = 0
                            idbio = result.rows[0].id
                            console.log(idbio);
                            if (error) {
                                response.send(400, {
                                    success: false,
                                    result: `Error GET id biodata`
                                })
                            } else {
                                const query = `INSERT INTO m_user(
                                    biodata_id, role_id, email, password, login_attempt, created_by, created_on, is_delete
                                    ) VALUES ('${idbio}', '${role_id}', '${email}', '${password}', 0, '${id}', now(), false)`
                                pool3.query(query, (error, result) => {
                                    if (error) {
                                        response.send(400, {
                                            success: false,
                                            result: `Error insert ke tabel m_user`
                                        })
                                    } else {
                                        response.send(200, {
                                            success: true,
                                            result: `Registrasi ${email} berhasil`
                                        })
                                    }
                                })
                            }
                        })
                    }
                })

            }

            
        })

    })

    /*----- LOGIN -----*/

    app.post("/api/auth/login", (request, response) => {
        const {
            email,
            password
        } = request.body;

        // const user = await User.findOne({ email });
        const query = `SELECT mu.id, mu.email, mu.password, mu.role_id, mu.login_attempt, mu.biodata_id, mb.fullname
            FROM m_user mu
            JOIN m_biodata mb
            ON mu.biodata_id = mb.id
            WHERE mu.is_delete = false AND mu.email = '${email}'`

        pool3.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error
                })
            } else {
                // if (result.rows[0].password === await bcrypt.compare(request.body.password, 10)) {
                if (result.rowCount < 1) {
                    response.send(401, {
                        success: false,
                        result: 'Email atau Password salah'
                    })
                } else {
                    // if (bcrypt.compare(request.body.password == result.rows[0].password)) {
                    if (password == result.rows[0].password) {
                        response.send(200, {
                            success: true,
                            result: result.rows[0]
                        })
                        const id = result.rows[0].id
                        let login_attempt = result.rows[0].login_attempt
                        pool3.query(`UPDATE m_user SET login_attempt = ${login_attempt + 1}, last_login = now() WHERE id = '${id}'`)
                    } else {
                        response.send(401, {
                            success: false,
                            result: 'Password Salah'
                        })
                    }
                }
                console.log(result);
            }
        })

    })

}