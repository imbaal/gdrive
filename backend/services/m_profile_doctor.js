const { request, response } = require("express");

module.exports = exports = (app, pool3) => {

    app.get('/api/profile_doctor_id/:biodata_id', (request, response) => {
        const biodata_id = request.params.biodata_id;
        const query = `SELECT Md.biodata_id, Md.id, Mb.image, Mb.image_path, Mb.fullname, Md.str, Td.specialization_id, COALESCE(Ms.name,'Belum Menambahkan Spesialisasi') AS name
        FROM public.m_doctor Md
        JOIN public.m_biodata Mb
        ON Md.biodata_id = Mb.id
        LEFt JOIN public.t_current_doctor_specialization Td
        ON Td.doctor_id = Md.id
        LEFT JOIN public.m_specialization Ms
        ON Ms.id = Td.specialization_id
        WHERE Md.biodata_id = ${biodata_id} AND Mb.is_delete = FALSE`

        pool3.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                let doctor = result.rows[0]
                const image = doctor.image;
                const str = Buffer.from(image).toString('base64')
                response.send(200, {
                    success: true,
                    result: {
                        ...doctor,
                        image: str
                    }
                });
            }
        });
    });

    app.get('/api/tindakan_medis/:id', (request, response) => {
        const id = request.params.id;
        const query = `SELECT Md.id, Mb.fullname, Tdt.name AS tindakan
        FROM public.m_doctor Md
        JOIN public.m_biodata Mb ON Md.biodata_id = Mb.id
        JOIN t_doctor_treatment Tdt ON Tdt.doctor_id = Md.id
        WHERE Md.id = ${id} AND Tdt.is_delete = FALSE
        `
        pool3.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows,
                });
            }
        });
    });

    app.get('/api/pendidikan_dokter/:id', (request, response) => {
        const id = request.params.id;
        const query = `SELECT Md.id, Mb.fullname, Mde.institution_name, Mde.major, Mde.start_year
        FROM public.m_doctor Md
        JOIN public.m_biodata Mb ON Md.biodata_id = Mb.id
        JOIN m_doctor_education Mde ON Mde.doctor_id = Md.id
        WHERE Md.id = ${id} AND Mde.is_delete = FALSE`

        pool3.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows,
                });
            }
        });
    });

    app.get('/api/riwayat_praktek/:id', (request, response) => {
        const id = request.params.id;
        const query = `SELECT Md.id, Mb.fullname, Mmfc.name AS category, Mmf.name AS faskes_name, Ml.name AS location, Tdo.specialization,
        EXTRACT(YEAR FROM Tdo.created_on) AS tahun_mulai, COALESCE(CAST(EXTRACT(YEAR FROM Tdo.deleted_on) AS VARCHAR),'sekarang') AS tahun_selesai
        FROM public.m_doctor Md
        JOIN public.m_biodata Mb ON Md.biodata_id = Mb.id
        JOIN public.t_doctor_office Tdo ON Tdo.doctor_id = Md.id
        JOIN public.m_medical_facility Mmf ON Mmf.id = Tdo.medical_facility_id
        JOIN public.m_medical_facility_category Mmfc ON Mmfc.id = Mmf.medical_facility_category_id
        JOIN public.m_location Ml ON Ml.id = Mmf.location_id
        WHERE Md.id = ${id} AND Md.is_delete = FALSE`
        
        pool3.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows,
                });
            }
        });
    });

    app.get('/api/biodata_doctor', (request, response) => {
        const query = `SELECT Md.id, Md.biodata_id, Mb.fullname, mb.image, mb.image_path
        FROM public.m_doctor Md
        LEFT JOIN public.m_biodata Mb
        ON Md.biodata_id = Mb.id`

        pool3.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows,
                });
            }
        });
    });

    app.put('/api/profile_doctor/:biodata_id', (request, response) => {
        const biodata_id = request.params.id
        const { image_path } = request.body
        const query = `UPDATE public.m_biodata SET image_path = ${image_path}
        modified_by = 1, modified_on = now()`

        pool3.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows,
                });
            }
        });
    });

    app.get('/api/janji_doctor/:id', (request, response) => {
        const id = request.params.id;
        const query = `SELECT COUNT(tdo.doctor_id) AS jumlah_janji
        FROM public.t_appointment ta
        JOIN public.t_doctor_office tdo ON tdo.id = ta.doctor_office_id
        WHERE tdo.doctor_id = ${id}
        GROUP BY tdo.doctor_id`

        pool3.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows[0],
                });
            }
        });
    });

    app.put('/api/edit_foto_dokter/:id', (request,response) => {
        const id = request.params.id
        const { image } = request.body

        const query = `UPDATE m_biodata
        SET image = decode(
            '${image}',
            'base64'
        )
        WHERE id = ${id}`

        pool3.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: `Image Updated`,
                });
            }
        });
    });
}