module.exports = exports = (app, pool) => {

    app.get('/pasien/golongandarah', (request, response) => {
        const query = `select * from m_blood_group
        where is_delete =false`

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                })
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows
                })

            }
        })
    })

    app.post('/pasien/addgolongandarah', (request, response) => {
        const { code, descrtiption } = request.body
        const query = `insert into public.m_blood_group(
            code,descrtiption,created_by,created_on,is_delete)
            values ( '${code}', '${descrtiption}','2', 'now()', false)`

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error
                })
            } else {
                response.send(200, {
                    success: true,
                    result: `data berhasil ditambahkan`
                })
            }
        })
    })

    app.put('/pasien/editgolongandarah/:id', (request, response) => {
        const indeks = request.params.id
        const { code, descrtiption } = request.body
        const query = `update m_blood_group set
        code = '${code}',
        descrtiption = '${descrtiption}',
        modified_by = '4',
        modified_on = 'now()'
        where id = ${indeks} `

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                })
            } else {
                response.send(200, {
                    success: true,
                    result: `data berhasil di update`
                })
            }
        })
    })

    app.put('/pasien/deletegolongandarah/:id', (request, response) => {
        const indeks = request.params.id
        console.log(indeks)
        const query = `update m_blood_group set
        is_delete  =true,
        deleted_by  = 1,
        deleted_on  = 'now()'
        where  id = ${indeks}`

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                })
            } else {
                response.send(200, {
                    success: true,
                    result: `data telah berhasil dihapus`
                })



            }
        })
    })


    app.get('/pasien/carigolongandarah/:cari', (request, response) => {
        let mencari = request.params.cari
        const query = `	select * from m_blood_group
        where is_delete =false and (code like lower ('${mencari}%') or code like lower('%${mencari}%') or descrtiption like lower('${mencari}%') or descrtiption like lower('%${mencari}%') or
        code like upper ('${mencari}%') or code like upper('%${mencari}%') or descrtiption like upper('${mencari}%') or descrtiption like upper('%${mencari}%'))`

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                })
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows
                })

            }
        })
    })
}
