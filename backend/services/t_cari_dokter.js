module.exports = exports = (app, db) => {
    app.get('/api/getdatadokter', (req, res) => {
        const query = `SELECT md.id,mb.fullname, ms.name as "Spesialisasi", mm.id as "idrs",mm.name as "RumahSakit", ml.name  as "Lokasi",mm.full_address, tdt.id as "idtreatment",tdt.name as "idtreatment"
        FROM m_biodata mb
        JOIN m_doctor md
        ON md.biodata_id=mb.id
        JOIN t_current_doctor_specialization tcds
        ON tcds.doctor_id=md.id
        JOIN m_specialization ms
        ON tcds.specialization_id=ms.id
        JOIN t_doctor_office tdo
        ON tdo.doctor_id=md.id
        JOIN m_medical_facility mm
        ON mm.id=tdo.medical_facility_id
        JOIN m_location ml
        ON mm.location_id=ml.id
        JOIN m_location_level mll
        ON mll.id=ml.location_level_id
		JOIN t_doctor_treatment tdt
		ON md.id= tdt.doctor_id
        where md.is_delete=true 
        order by md.id asc`
        db.query(query, (err, result) => {
            if (err) {
                res.send(400, {
                    status: false,
                    result: err
                })
            } else {
                newDoctor = result.rows
                newDoctor1 = []
                newRumahSakit = []
                newTreatment = []
                for (let i = 0; i < newDoctor.length; i++) {
                    if (i < newDoctor.length - 1) {
                        if (newDoctor[i].id == newDoctor[i + 1].id) {
                            newRumahSakit.push({
                                rumahsakit: newDoctor[i].RumahSakit,
                                address: newDoctor[i].full_address,
                                lokasi: newDoctor[i].Lokasi
                            })

                        } else {
                            newRumahSakit.push({
                                rumahsakit: newDoctor[i].RumahSakit,
                                address: newDoctor[i].full_address,
                                lokasi: newDoctor[i].Lokasi
                            })

                            newDoctor1.push({
                                id: newDoctor[i].id,
                                name: newDoctor[i].fullname,
                                spesialisasi: newDoctor[i].Spesialisasi,
                                RumahSakit: newRumahSakit,

                            })
                            newRumahSakit = []
                        }
                    } else {
                        newRumahSakit.push({
                            rumahsakit: newDoctor[i].RumahSakit,
                            address: newDoctor[i].full_address,
                            lokasi: newDoctor[i].Lokasi
                        })

                        newDoctor1.push({
                            id: newDoctor[i].id,
                            name: newDoctor[i].fullname,
                            spesialisasi: newDoctor[i].Spesialisasi,
                            RumahSakit: newRumahSakit,

                        })
                        newRumahSakit = []
                    }
                }
                res.send(200, {
                    status: true,
                    result: newDoctor1
                })
            }

        })
    })

    app.get('/api/getdatadokter1', (req, res) => {
        let fullname = req.query.fullname
        fullame = "" ? "" : fullname
        let spesialisasi = req.query.spesialisasi
        spesialisasi = "" ? "" : spesialisasi
        let lokasi = req.query.lokasi
        lokasi = "" ? "" : lokasi
        let treatment = req.query.treatment
        treatment = "" ? "" : treatment
        let query = `		
        select md.id,mm.id as "idrs",mm.name as "RumahSakit", mll.name  as "Lokasi",mm.full_address
        From m_doctor md
        JOIN t_doctor_office tdo
        ON tdo.doctor_id=md.id
        JOIN m_medical_facility mm
        ON mm.id=tdo.medical_facility_id
        JOIN m_location ml
        ON mm.location_id=ml.id
        JOIN m_location_level mll
        ON mll.id=ml.location_level_id
        where (md.is_delete=true 
        and  lower(mll.name) like lower('%${lokasi}%'))
       order by md.id asc`
        db.query(query, (err, result) => {
            if (err) {
                res.send(400, {
                    status: false,
                    result: err
                })
            } else {
                newDoctor = []
                newRumahSakit = []
                newTreatment = []
                dataLokasi = []
                FinalDoctor = []
                dataLokasiDoc = result.rows
                if (result.rowCount != 0) {
                    query = `SELECT md.id,mb.fullname, ms.name as "Spesialisasi", mb.image_path
                   FROM m_biodata mb
                   JOIN m_doctor md
                   ON md.biodata_id=mb.id
                   JOIN t_current_doctor_specialization tcds
                   ON tcds.doctor_id=md.id
                   JOIN m_specialization ms
                   ON tcds.specialization_id=ms.id
                   where md.is_delete=true and 
                   lower(mb.fullname) like lower('%${fullname}%') and 
                   lower(ms.name) like lower('%${spesialisasi}%')
                   order by md.id asc`
                    db.query(query, (err, result) => {
                        if (err) {
                            res.send(400, {
                                status: false,
                                result: err
                            })
                        } else {
                            dataDoctor = result.rows
                            if (result.rowCount != 0) {
                                query = `select md.id,tdt.name as "nama_treatment"
                                       From m_doctor md
                                       JOIN t_doctor_treatment tdt
                                       ON md.id= tdt.doctor_id
                                       where md.is_delete=true 
                                       order by md.id asc`
                                db.query(query, (err, result) => {
                                    if (err) {
                                        res.send(400, {
                                            status: false,
                                            result: err
                                        })
                                    } else {
                                        datatreatment = result.rows
                                        for (i = 0; i < dataDoctor.length; i++) {
                                            for (j = 0; j < dataLokasiDoc.length; j++) {
                                                if (dataDoctor[i].id == dataLokasiDoc[j].id) {
                                                    newRumahSakit.push({
                                                        Nama: dataLokasiDoc[j].RumahSakit,
                                                        Alamat: dataLokasiDoc[j].full_address,
                                                        Lokasi: dataLokasiDoc[j].Lokasi,
                                                    })
                                                }
                                            }
                                          
                                            newDoctor.push({
                                                id: dataDoctor[i].id,
                                                image: dataDoctor[i].image_path,
                                                fullname: dataDoctor[i].fullname,
                                                spesialisasi: dataDoctor[i].Spesialisasi,
                                                RumahSakit: newRumahSakit
                                            })
                                            newRumahSakit = []
                                        }

                                        for (i = 0; i < newDoctor.length; i++) {
                                            for (j = 0; j < datatreatment.length; j++) {
                                                if (newDoctor[i].id == datatreatment[j].id) {
                                                    newTreatment.push(datatreatment[j].nama_treatment)
                                                }
                                            }
                                            FinalDoctor.push({
                                                ...newDoctor[i],
                                                treatment: newTreatment
                                            })
                                            newTreatment = []
                                        }
                                
                                        res.send(200, {
                                            status: true,
                                            result: FinalDoctor
                                        })
                                    }
                                })

                            } else {
                                res.send(200, {
                                    status: true,
                                    result: result.rows
                                })
                            }
                        }
                    })
                } else {
                    res.send(200,{
                        status: true,
                        result:[]
                    })
                }
            }
        })

    })

    app.get('/api/getdatalocation', (req, res) => {
        const query = 'SELECT id, name FROM m_location_level where is_delete=true'
        db.query(query, (err, result) => {
            if (err) {
                res.send(400, {
                    status: false,
                    result: err
                })
            } else {
                res.send(200, {
                    status: true,
                    result: result.rows
                })
            }
        })
    })

    app.get('/api/getspec', (req, res) => {
        const query = 'SELECT id,name FROM m_specialization where is_delete=true ORDER BY id ASC'
        db.query(query, (err, result) => {
            if (err) {
                res.send(400, {
                    status: false,
                    result: err
                })
            } else {
                res.send(200, {
                    status: true,
                    result: result.rows
                })
            }
        })
    })

    app.get('/api/gettreat', (req, res) => {
        const query = 'select name  from t_doctor_treatment ORDER BY name asc'
        db.query(query, (err, result) => {
            if (err) {
                res.send(400, {
                    status: false,
                    result: err
                })
            } else {
                treatment = result.rows
                newTreatment = []
                count = 0
                for (let i = 0; i < treatment.length; i++) {
                    if (i < treatment.length - 1) {
                        if (treatment[i].name != treatment[i + 1].name) {
                            newTreatment.push({
                                name: treatment[i].name
                            })
                        }
                    } else {
                        newTreatment.push({
                            name: treatment[i].name
                        })
                    }
                }
                res.send(200, {
                    result: newTreatment,
                })
            }
        })
    })


}