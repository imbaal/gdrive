module.exports = exports = (app, pool) => {
    app.get('/pembayaran', (request, response) => {
        const query = `SELECT id,customer_id, card_number, validity_period, cvv, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete,concat(extract(year from validity_period) ,'/', extract(month from validity_period)) as valid 
        FROM public.t_customer_registered_card where is_delete = false`

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                })
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows
                })

            }
        })
    })

    app.get('/pembayaran/:id', (request, response) => {
        const indeks = request.params.id
        console.log(indeks)
        const query = `SELECT id,customer_id, card_number, validity_period, cvv, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete,concat(extract(year from validity_period) ,'/', extract(month from validity_period)) as valid 
        FROM public.t_customer_registered_card 
		where is_delete = false and customer_id = ${indeks}`

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                })
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows
                })

            }
        })
    })


    app.post('/pembayaran/tambahpembayaran', (request, response) => {
        const {card_number,validity_period ,cvv } = request.body
        const query = `INSERT INTO public.t_customer_registered_card(
            customer_id, card_number, validity_period, cvv, created_by, created_on, is_delete)
            VALUES (1, '${card_number}', '${validity_period}-1', '${cvv}', 1, 'now()', false)`

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error
                })
            } else {
                response.send(200, {
                    success: true,
                    result: `data berhasil ditambahkan`
                })
            }
        })
    })

    app.put('/pembayaran/deletepembayaran/:id', (request, response) => {
        const indeks = request.params.id
        console.log(indeks)
        const query = `update public.t_customer_registered_card set
        is_delete   = true,
        deleted_by  = 1,
        deleted_on  = 'now()'
        where  id = ${indeks}`

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                })
            } else {
                response.send(200, {
                    success: true,
                    result: 'data telah berhasil dihapus'
                })



            }
        })
    })
}