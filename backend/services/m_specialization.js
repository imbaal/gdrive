const { request, response } = require("express")

module.exports = exports = (app, pool3) => {
    app.get('/api/spesialisasi_dokter', (request, response) => {
        const query = `SELECT * FROM m_specialization WHERE is_delete = FALSE ORDER BY name`

        pool3.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows,
                });
            }
        });
    });

    app.get('/api/spesialisasi_dokter/:search', (request, response) => {
        const search = request.params.search;
        const query = `SELECT * FROM m_specialization WHERE LOWER(name) LIKE LOWER('%${search}%') AND is_delete = FALSE ORDER BY name`

        pool3.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error
                });
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows,
                });
            }
        });
    })

    app.post('/api/spesialisasi_dokter', (request, response) => {
        const { name } = request.body;

        const query = `INSERT INTO public.m_specialization
            (name, created_by, created_on)
            VALUES ('${name}', '1', 'now()')`;

        pool3.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: `Data Dokter ${name} saved`
                });
            }
        });
    });

    app.put('/api/ubahspesialisasi_dokter/:id', (request, response) => {
        const { name } = request.body;
        const id = request.params.id;

        const query = `UPDATE public.m_specialization SET name = '${name}', modified_by = '1', modified_on = 'now()' WHERE id = ${id}`;

        pool3.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: `Data Dokter ${name} updated`
                });
            }
        });
    });

    app.put('/api/hapusspesialisasi_dokter/:id', (request, response) => {
        const id = request.params.id;

        const query = `UPDATE public.m_specialization SET is_delete = TRUE, deleted_by = '1', deleted_on = 'now()' WHERE id = ${id}`;

        pool3.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: `Data Spesialisasi Dokter ${id} deleted`
                });
            }
        });
    });

}


