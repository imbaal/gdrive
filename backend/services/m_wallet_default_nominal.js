module.exports = exports = (app, db) => {
    app.post('/api/adddefaultnominal/:nominal', (req, res) => {
        const nominal = req.params.nominal
        const query = `INSERT INTO public.m_wallet_default_nominal(
            nominal, created_by, created_on,is_delete)
            VALUES (${nominal}, 1, now(), true); `
        db.query(query, (err, result) => {
            if (err) {
                res.send(400, err)
            } else {
                res.send(200, result.rows)
            }
        })
    })

    app.put('/api/geteditdefaultnominal/:id', (req, res) => {
        const id = req.params.id
        const {nominal} = req.body
        // const nominal = 10000
    
        const query = `UPDATE public.m_wallet_default_nominal SET nominal=${nominal},
        modified_by=2, modified_on= now()WHERE id =${id}`
        db.query(query, (error, result) => {
            if (error) {
                res.send(400, error)
            } else {
                res.send(200, {
                    status: 200,
                    result: nominal
                })
            }
        })
    })
    app.put('/api/deletedefault/:id',(req, res) => {
        const id = req.params.id
        const query = `UPDATE public.m_wallet_default_nominal SET deleted_by=3,deleted_on=now(),is_delete=false WHERE id =${id}`
        db.query(query, (error, result) => {
            if (error) {
                res.send(400, error)
            } else {
                res.send(200, {
                    result: 'Data Berhasil Dihapus'
                })
            }
        })
    })
    app.post('/api/getsearch/:nominal', (req, res) => {
        const nominal = req.params.nominal
        db.query(`SELECT * FROM m_wallet_default_nominal WHERE CAST(nominal AS TEXT) LIKE '${nominal}%' AND is_delete=true`,
            (err, result) => {
                if (err) {
                    res.send(400, err)
                } else {
                    res.send(200, result.rows)
                }
            })
    })

    // app.get('/pembayaran', (request, response) => {
    //     const query = `SELECT id,customer_id, card_number, validity_period, cvv, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete,concat(extract(year from validity_period) ,'/', extract(month from validity_period)) as valid 
    //     FROM public.t_customer_registered_card where is_delete = false`

    //     db.query(query, (error, result) => {
    //         if (error) {
    //             response.send(400, {
    //                 success: false,
    //                 result: error,
    //             })
    //         } else {
    //             response.send(200, {
    //                 success: true,
    //                 result: result.rows
    //             })

    //         }
    //     })
    // })

    // app.post('/pembayaran/tambahpembayaran', (request, response) => {
    //     const {card_number,validity_period ,cvv } = request.body
    //     const query = `INSERT INTO public.t_customer_registered_card(
    //         customer_id, card_number, validity_period, cvv, created_by, created_on, is_delete)
    //         VALUES (1, '${card_number}', '${validity_period}-1', '${cvv}', 1, 'now()', false)`

    //     db.query(query, (error, result) => {
    //         if (error) {
    //             response.send(400, {
    //                 success: false,
    //                 result: error
    //             })
    //         } else {
    //             response.send(200, {
    //                 success: true,
    //                 result: `data berhasil ditambahkan`
    //             })
    //         }
    //     })
    // })

    // app.put('/pembayaran/deletepembayaran/:id', (request, response) => {
    //     const indeks = request.params.id
    //     console.log(indeks)
    //     const query = `update public.t_customer_registered_card set
    //     is_delete   = true,
    //     deleted_by  = 1,
    //     deleted_on  = 'now()'
    //     where  id = ${indeks}`

    //     db.query(query, (error, result) => {
    //         if (error) {
    //             response.send(400, {
    //                 success: false,
    //                 result: error,
    //             })
    //         } else {
    //             response.send(200, {
    //                 success: true,
    //                 result: `data telah berhasil dihapus`
    //             })



    //         }
    //     })
    // })

}