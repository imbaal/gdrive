const { db } = require("../config/dbconfig")

module.exports = exports = (app, pool) => {
  app.get('/api/getDataSaldo/:id', (req, res) => {
    let customer_id = req.params.id
    // customer_id = 1
    query = `select id,customer_id,balance from t_customer_wallet where customer_id=${customer_id}`
    pool.query(query, (error, result) => {
      if (error) {
        res.send(400)
      } else {
      
        res.send(200, result = result.rows)
     
      }
    })
  })

  app.post('/api/getWallettambah1/:id', (req, res) => {
    let customer_id = req.params.id
    let {nominal}=req.body
    console.log(nominal)
    // let nominal = req.params.nominal
    // let customer_id = req.body
    query = `select balance from t_customer_wallet where customer_id=${customer_id}`
  pool.query(query, (error, result) => {
    if (error) {
      res.send(400, {
        status: false,
        result:error
      })
    } else {
      balance = Object.values(result.rows[0])
      console.log(balance)
      let total = parseInt(nominal) + parseInt(balance)
      console.log(total)
      query = `UPDATE public.t_customer_wallet
          SET  balance=${total},modified_by=1, modified_on=now()
          WHERE customer_id=${customer_id};`
        pool.query(query, (error, result) => {
          if (error) {
            res.send(400, {
              status: false,
              result:'error dimari2'
            })
          } else {
            query=`INSERT INTO public.t_customer_custom_nominal(
              customer_id, nominal, created_by, created_on,is_delete)
              VALUES (${customer_id},${nominal},1,now(),true);`
            pool.query(query, (error, result) => {
              if(error){
                res.send(400, {
                  status: false,
                  result: 'error dimari3'
                })
              }else{
                res.send(200,{
                  result : 'Data berhasil ditambah'
                })
              }
            })
          }
        })
      }
    })
  })

  app.post('/api/getWallettambah/:id', (req, res) => {
    let customer_id = req.params.id
    let {nominal}=req.body
    console.log(nominal)
    // let nominal = req.params.nominal
    // let customer_id = req.body
    query = `select balance from t_customer_wallet where customer_id=${customer_id}`
    pool.query(query, (error, result) => {
      if (error) {
        res.send(400, {
          status: false,
          result:error
        })
      } else {
        balance = Object.values(result.rows[0])
        let total = parseInt(nominal) + parseInt(balance)
        query = `UPDATE public.t_customer_wallet
                SET   balance=${total}, created_by=1, created_on=now()
                WHERE customer_id=${customer_id};`
        pool.query(query, (error, result) => {
          if (error) {
            console.log('eror 2');
            res.send(400, {
              status: false,
            })
          } else {
            res.send(200, {
              status: true,
              result:'berhasil diupdate'
            })
          }
        })
      }
    })
  })
  
  app.get('/api/customnominal/:id',(req, res) => {
    let customer_id=req.params.id
    const query = `select id,nominal from t_customer_custom_nominal where customer_id=${customer_id} and is_delete = true`
    pool.query(query,(error,result) => {
      if(error) {
        res.send(400, {
          status: false,
          result:error
        })
      }else{
        res.send(200, result.rows)
      }
    })
  })

    app.post('/api/tambahnominal', (req, res) => {
      let { customer_id, nominal } = req.body
      const query = `INSERT INTO public.t_customer_custom_nominal(
          customer_id, nominal, created_by, created_on)
          VALUES (${customer_id},${nominal},1,now());`
      pool.query(query, (error, result) => {
        if (error) {
         
          res.send(400, {
            status: false,
            result: error
          })
        } else {
          res.send(200, 'data berhasil ditambah')
        }
      })
    })

    app.get('/api/nominaldefault', (req, res) => {
      const query = `select id, nominal from m_wallet_default_nominal where  is_delete=true order by nominal asc`
      pool.query(query, (error, result) => {
        if (error) {
          res.send(400, {
            status: false,
            result: error
          })
        } else {
          res.send(200, result.rows)
        }
      })
    })

    app.get('/api/getcustomerid/:id',(req, res) => {
      const id=req.params.id
      const query = `select id from m_customer where biodata_id=${id}`
      db.query(query, (error, result) => {
        if(error) {
          res.send(400, {
            result:error
          })
        }else{
          res.send(200, {
            result:result.rows[0].id
          })
        }
        
      })
    })

  
}