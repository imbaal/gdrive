const {
    request,
    response,
    query
} = require("express")

module.exports = exports = (app, pool5) => {
    app.get("/api/role", (request, response) => {
        const query = `SELECT * FROM m_role`

        pool5.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error
                })
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    })

    app.get("/api/selectRole/:id", (request, response) => {
        const id  = request.params.id

        const query = `SELECT r.id, r.name, r.code FROM m_role r WHERE r.id = '${id}'`

        pool5.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error
                })
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    })


}