module.exports = exports = (app, pool) => {
app.get('/cariobat/popup',(request,response)=>{
    let kategori = request.query.kategori
    let katakunci = request.query.katakunci
    let jenisobat = request.query.segmentation
    let hmin = parseInt(request.query.price_min)
    let hmax = parseInt(request.query.price_max)
    let halaman = parseInt(request.query.page)
    console.log(kategori,katakunci, jenisobat,hmin,hmax,halaman)
    const query = `select m.id, m.name, m.manufacturer, m.indication, m.dosage, m.contraindication, m.caution, m.packaging, m.price_max, m.price_min, m.image, m.image_path
    FROM public.m_medical_item m
    join public.m_medical_item_category c
    on m.medical_item_category_id = c.id
    join public.m_medical_item_segmentation s
    on m.medical_item_segmentation_id =s.id
    where m.is_delete = false and
    (m.name like lower ('${katakunci}%') or m.name like lower('%${katakunci}%')or m.indication like lower('${katakunci}%') or m.indication like
    lower('%${katakunci}%') or m.indication like upper('${katakunci}%') or m.indication like upper('%${katakunci}%')or m.name like upper ('${katakunci}%') or m.name like upper ('%${katakunci}%')) and
    c.id = ${kategori} and 
    s.id = ${jenisobat} and
    m.price_max  < ${hmax} and
    m.price_min  > ${hmin}
    offset ${(halaman-1)*4}
    limit ${halaman*4}`
    pool.query(query,(error,result)=>{
        if (error){
            response.send(400,{
                success: false,
                result: error,
            })
        }else{
            response.send(200,{
                success:true,
                result:result
            })
        }
    })
})

app.get('/cariobat/awalpopup',(request,response)=>{
    const query = `SELECT id, name, created_by, created_on, modified_by, modified_on, deleted_by, deleted_on, is_delete
	FROM public.m_medical_item_category;`
    pool.query(query,(error,result)=>{
        if (error){
            response.send(400,{
                success: false,
                result: error,
            })
        }else{
            response.send(200,{
                success:true,
                result:result.rows
            })
        }
    })    
})

}