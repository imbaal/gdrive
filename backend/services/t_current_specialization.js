module.exports = exports = (app, pool3) => {

    app.get('/api/Tspesialisasi_doctor/:doctor_id', (request, response) => {
        const doctor_id = request.params.doctor_id;

        const query = `SELECT doctor_id, specialization_id FROM public.t_current_doctor_specialization
        WHERE doctor_id = ${doctor_id}`

        pool3.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows[0],
                });
            }
        });
    });
  
    
    app.post('/api/Tspesialisasi_doctor/', (request, response) => {
        const { doctor_id, specialization_id } = request.body

        const query = `INSERT INTO public.t_current_doctor_specialization (doctor_id, specialization_id, created_by, created_on)
        VALUES ('${doctor_id}', '${specialization_id}', '1', 'now()')`

        pool3.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: `Data ${specialization_id} Saved`,
                });
            }
        });
    });

    app.put('/api/Tspesialisasi_doctor/', (request, response) => {
        // const doctor_id = request.params.doctor_id;
        const { doctor_id, specialization_id } = request.body
        const query = `UPDATE public.t_current_doctor_specialization SET specialization_id = '${specialization_id}', modified_by = '1', modified_on = 'now()'
        WHERE doctor_id = ${doctor_id}`

        pool3.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: `Data ${specialization_id} updated`,
                });
            }
        });
    });
}