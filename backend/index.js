const express = require('express')
const bodyParser = require('body-parser')
const {
    request,
    response
} = require('express')
const cors = require('cors')

global.config = require('./config/dbconfig')

const app = express()
const port = 3000

app.use(cors())
app.use(bodyParser.json())
app.use(
    bodyParser.urlencoded({
        extended: true
    })
)

app.get('/', (request, response) => {
    response.json({
        info: 'Node.js, Express, and Postgres API'
    })
})

require('./services/m_specialization')(app, global.config.pool3)
require('./services/m_profile_doctor')(app, global.config.pool3);
require('./services/t_current_specialization')(app, global.config.pool3)
require('./services/m_user')(app, global.config.pool3);
require('./services/t_detail_doctor')(app, global.config.pool3);

// Pool 4
// require('./services/m_user')(app, global.config.pool4);
// require('./services/m_role')(app, global.config.pool4);

// Pool 5
// require('./services/m_user')(app, global.config.pool5);
require('./services/m_role')(app, global.config.pool5);

require('./services/m_user')(app, global.config.db);
require('./services/m_role')(app, global.config.pool4);

require('./services/m_payment_method')(app, global.config.db)
require('./services/m_blood_group')(app, global.config.pool)
require('./services/m_customer_wallet')(app, global.config.db)
require('./services/m_wallet_default_nominal')(app, global.config.db)
require('./services/m_bank')(app, global.config.pool2);
require('./services/t_tab_pembayaran')(app, global.config.pool)
require('./services/t_pasien')(app, global.config.pool2);
require('./services/t-cariobat')(app,global.config.pool)

require('./services/t_cari_dokter')(app, global.config.db);
// // require('./services/t_tab_pembayaran')(app, global.config.poo3)
// require('./services/t_tab_pembayaran')(app, global.config.pool)
// require('./services/m_user')(app, global.config.pool4);
require('./services/t_pasien')(app, global.config.pool2);

require('./services/t_cari_dokter')(app, global.config.db);
// require('./services/m_user')(app, global.config.pool3);
// require('./services/t_tab_pembayaran')(app, global.config.pool3)


app.listen(port, () => {
    console.log(`App running on port ${port}`)
})